﻿using KIM_SmokeTest.BaseClass;
using KIM_SmokeTest.UtilityClasses;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Data;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using TechTalk.SpecFlow;

namespace KIM_SmokeTest.FeatureSteps
{
    internal class EnrolmentsPage : BaseApplicationPage
    {
        public EnrolmentsPage(IWebDriver driver) : base(driver)
        { }
        public By Enrolments_Element => By.XPath("//span[@class='status enrolment-tab-status']");
        public By AddNewEnrolment_Element => By.XPath("//input[@class='btn blue add-enrol float-right']");

        public By GivenName_Enroll => By.XPath("//input[@id='enrolDetailsGName']");
        public By FamilyName_Enroll => By.CssSelector("#enrolDetailsFName");
        public By Sex_dropdown => By.CssSelector("#enrolOptGender");
        public IWebElement DOB_Enroll => Driver.FindElement(By.CssSelector("#enrolDetailsDOB"));
        public By UnitNum_Enroll => By.CssSelector("#enrolNumber");
        public By StreetName_Enroll => By.CssSelector("#enrolStreet");
        public IWebElement StreetType_Enroll => Driver.FindElement(By.CssSelector("#enrolStreetType"));
        public IWebElement Suburb_Enroll => Driver.FindElement(By.CssSelector("#enrolSuburb"));
        public IWebElement State_Enroll => Driver.FindElement(By.CssSelector("#enrolState"));
        public By Postcode_Enroll => By.CssSelector("#enrolPostcode");
        public By DateCommenced_Enroll => By.CssSelector("#enrolDateAttFundedProCommenced");
        public By NextBtn_Enroll => By.XPath("//form[@id='enrolmentAddEditForm']//button[@name='btnNext'][contains(text(),'Next')]");
        //RadioButton 
        public By IndigenousStatus => By.CssSelector("#statusNo");
        //Language Dropdown
        public IWebElement Language_Element => Driver.FindElement(By.CssSelector("#langHome"));
        //Immunisation status
        public IWebElement Immunisation_Status => Driver.FindElement(By.CssSelector("#immunisationStatusChild"));
        //Group attended
        public IWebElement Group_Attended => Driver.FindElement(By.CssSelector("#groupOfChild"));
        //Early start Kinder 
        public IWebElement EarlyStart_Element => Driver.FindElement(By.CssSelector("#earlyStartGrantNo"));
        //next btn
        public By NextBtn_Element => By.XPath("//form[@id='enrolmentAddEditForm']//button[@name='btnNext'][contains(text(),'Next')]");
        //public By Checkbox_Element => By.CssSelector("#enrolmentIgnoreErrors");

        //AdultA Dropdown
        public IWebElement AdultA_HighestYear => Driver.FindElement(By.CssSelector("#SFOEAnswer1"));
        public IWebElement AdultA_Qualification => Driver.FindElement(By.CssSelector("#SFOEAnswer2"));
        public IWebElement AdultA_Occupation => Driver.FindElement(By.CssSelector("#SFOEAnswer3"));

        //AdultB Dropdown

        public IWebElement AdultB_HighestYear => Driver.FindElement(By.CssSelector("#SFOEAnswer4"));
        public IWebElement AdultB_Qualification => Driver.FindElement(By.CssSelector("#SFOEAnswer5"));
        public IWebElement AdultB_Occupation => Driver.FindElement(By.CssSelector("#SFOEAnswer6"));

        //next btn
        public By NextBtn => By.XPath("//form[@id='enrolmentAddEditForm']//button[@name='btnNext'][contains(text(),'Next')]");

        //Eligibility
        public IWebElement Checkbox_Eligibility => Driver.FindElement(By.CssSelector("#feeSubCHCC"));
        By ChildParentRelationship => By.XPath("//li[contains(text(),'Informal kinship care')]");
        public IWebElement ChildLive => Driver.FindElement(By.XPath("//span[@class='dropdownlisttext']"));
        //Save
        By SaveButton_Enroll => By.XPath("//form[@id='enrolmentAddEditForm']//input[@name='btnSave']");
        //Upload bulk enrolments
        By UploadEnrolments_Element => By.XPath("//a[contains(text(),'Upload Multiple Enrolments +')]");
        By ChooseFile => By.XPath("//span[contains(text(),'Choose file...')]");
        public By ConfirmUploadFile_Element { get; private set; }
        public By SearchEnrolment_Element => By.XPath("//div[@id='DataTables_Table_1_filter']//input");
        public By DeleteEnrolment_Element => By.XPath("//tbody[@id='EnrolmentSummaryDataGrid']//button[@class='btn blue delete'][contains(text(),'Remove')]");
        public By EditEnrolment_Element => By.XPath("//tbody[@id='EnrolmentSummaryDataGrid']//button[@class='btn blue edit'][contains(text(),'Edit')]");
        public By Element => By.XPath("//tbody[@id='EnrolmentSummaryDataGrid']//button[contains(@class,'btn blue delete')][contains(text(),'Remove')]");
        public By Dropdown_Element => By.XPath("//select[@id='enrolReasonLeaving']");
        public IWebElement RemoveButton_Enrolment => Driver.FindElement(By.XPath("//form[@id='enrolmentDeleteForm']//input[@class='btn blue btn-primary delete submit']"));
        public By FinalDate_ElementIcon => By.XPath("//div[@id='groupEnrolDepartureDate']//span[@class='input-group-addon']");
        public IWebElement FinalDate_Select => Driver.FindElement(By.XPath("//td[@class='today active day']"));
        public IWebElement EskGrant_Yes_Radio => Driver.FindElement(By.CssSelector("#earlyStartGrantYes"));        
        public By EskGrant_Cp_Radio => By.CssSelector("#earlyStartGrantChoice2");
        public By EskGrant_Program_Radio => By.CssSelector("#proDelByServiceYes");
        public By EskGrant_Apply_Radio => By.CssSelector("#isEligibleKindergartenFeeYes");
        internal void AddNewEnrolments()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            wait.Until(x => x.FindElement(Enrolments_Element));
            Driver.FindElement(Enrolments_Element).Click();
            Thread.Sleep(3000);
            Driver.FindElement(AddNewEnrolment_Element).Click();
        }

        internal void FillNewEnrolmentForm_KFS(Table table)
        {
            var dataTable = TableExtensions.ToDataTable(table);
            foreach (DataRow row in dataTable.Rows)

            {
                WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
                wait.Until(x => x.FindElement(GivenName_Enroll));
                //random generator of GivenName
                Random generatorGn = new Random();
                int iGn = generatorGn.Next(1, 10000000);
                string sGn = iGn.ToString().PadLeft(7, '0');
                string eGivenName = "EGN." + sGn;
                //removed Driver.FindElement(GivenName_Enroll).SendKeys(row.ItemArray[0].ToString());
                Driver.FindElement(GivenName_Enroll).SendKeys(eGivenName);

                wait.Until(x => x.FindElement(FamilyName_Enroll));
                //random generator of FamilyName
                Random generatorFn = new Random();
                int iFn = generatorFn.Next(1, 10000000);
                string sFn = iFn.ToString().PadLeft(7, '0');
                string eFamilyName = "Fn." + sFn;
                //removed Driver.FindElement(FamilyName_Enroll).SendKeys(row.ItemArray[1].ToString());
                Driver.FindElement(FamilyName_Enroll).SendKeys(eFamilyName);

                wait.Until(x => x.FindElement(Sex_dropdown));
                SelectElement Sex = new SelectElement(Driver.FindElement(Sex_dropdown));
                Sex.SelectByText(row.ItemArray[2].ToString());

                DOB_Enroll.Clear();
                DOB_Enroll.Click();
                //random generator DOB
                DateTime start = new DateTime((DateTime.Now.Year-5), 1, 1);
                DateTime end = new DateTime((DateTime.Now.Year-5), 12, 31);
                Random gen = new Random();
                int range = ((TimeSpan)(end - start)).Days;
                string dOB = start.AddDays(gen.Next(range)).ToString("dd/MM/yyyy");
                //removedDOB_Enroll.SendKeys(row.ItemArray[3].ToString());
                DOB_Enroll.SendKeys(dOB);
                
                wait.Until(x => x.FindElement(UnitNum_Enroll));
                //random generator of Unit number
                Random generatorUn = new Random();
                int iUn = generatorUn.Next(1, 1000000);
                string unitNum = iUn.ToString().PadLeft(6, '0');
                //removed Driver.FindElement(UnitNum_Enroll).SendKeys(row.ItemArray[4].ToString());
                Driver.FindElement(UnitNum_Enroll).SendKeys(unitNum);

                wait.Until(x => x.FindElement(StreetName_Enroll));
                //random generator of Street
                Random generatorStn = new Random();
                int iStn = generatorStn.Next(1, 1000000);
                string sStn = iStn.ToString().PadLeft(6, '0');
                string stnum = "Str-" + DateTime.Now.ToString("ddmmyyyy")+"_"+ sStn;
                //Driver.FindElement(StreetName_Enroll).SendKeys(row.ItemArray[5].ToString());
                Driver.FindElement(StreetName_Enroll).SendKeys(stnum);

                var SelectElement_StreetType = new SelectElement(StreetType_Enroll);
                SelectElement_StreetType.SelectByText(row.ItemArray[6].ToString());

                Suburb_Enroll.SendKeys(row.ItemArray[7].ToString());

                var SelectElement_State = new SelectElement(State_Enroll);
                SelectElement_State.SelectByText(row.ItemArray[8].ToString());

                wait.Until(x => x.FindElement(Postcode_Enroll));
                //random generator of Post Code
                Random generatorPc = new Random();
                int iPc = generatorPc.Next(1111, 9999);
                string postCode = iPc.ToString().PadLeft(3, '0');
                //removed Driver.FindElement(Postcode_Enroll).SendKeys(row.ItemArray[9].ToString());
                Driver.FindElement(Postcode_Enroll).SendKeys(postCode);

                wait.Until(x => x.FindElement(DateCommenced_Enroll));
                Driver.FindElement(DateCommenced_Enroll).SendKeys(row.ItemArray[10].ToString());
                wait.Until(x => x.FindElement(NextBtn_Enroll));
                Driver.FindElement(NextBtn_Enroll).Click();
                wait.Until(x => x.FindElement(IndigenousStatus));
                Driver.FindElement(IndigenousStatus).Click();

                var SelectElement_Language = new SelectElement(Language_Element);
                SelectElement_Language.SelectByText(row.ItemArray[11].ToString());

                
                ChildLive.Click();
                wait.Until(x => x.FindElement(ChildParentRelationship));
                Driver.FindElement(ChildParentRelationship).Click();

                var SelectElement_Immunisation = new SelectElement(Immunisation_Status);
                SelectElement_Immunisation.SelectByText(row.ItemArray[12].ToString());

                var SelectElement_Group = new SelectElement(Group_Attended);
                SelectElement_Group.SelectByText(row.ItemArray[13].ToString());
                EarlyStart_Element.Click();
                Driver.FindElement(NextBtn_Element).Click();

                var SelectElement_AdultAHighest = new SelectElement(AdultA_HighestYear);
                SelectElement_AdultAHighest.SelectByText(row.ItemArray[14].ToString());

                var SelectElement_AdultAQualification = new SelectElement(AdultA_Qualification);
                SelectElement_AdultAQualification.SelectByText(row.ItemArray[15].ToString());

                var SelectElement_AdultAOccupation = new SelectElement(AdultA_Occupation);
                SelectElement_AdultAOccupation.SelectByText(row.ItemArray[16].ToString());

                var SelectElement_AdultBHighest = new SelectElement(AdultB_HighestYear);
                SelectElement_AdultBHighest.SelectByText(row.ItemArray[17].ToString());

                var SelectElement_AdultBQualification = new SelectElement(AdultB_Qualification);
                SelectElement_AdultBQualification.SelectByText(row.ItemArray[18].ToString());

                var SelectElement_AdultBOccupation = new SelectElement(AdultB_Occupation);
                SelectElement_AdultBOccupation.SelectByText(row.ItemArray[19].ToString());
                wait.Until(x => x.FindElement(NextBtn));
                Driver.FindElement(NextBtn).Click();
                Checkbox_Eligibility.Click();
                wait.Until(x => x.FindElement(SaveButton_Enroll));
                Driver.FindElement(SaveButton_Enroll).Click();
                Thread.Sleep(4000);
                wait.Until(x => x.FindElement(AddNewEnrolment_Element));
                Driver.FindElement(AddNewEnrolment_Element).Click();
            }
        }

        internal void UploadEnrolmentFile() { 
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            wait.Until(x => x.FindElement(Enrolments_Element));
            Driver.FindElement(Enrolments_Element).Click();
            // Navigate to bulk enrolment window 
            wait.Until(x => x.FindElement(UploadEnrolments_Element));
            Driver.FindElement(UploadEnrolments_Element).Click();
            var winHandleBefore = Driver.CurrentWindowHandle;
            wait.Until(x => x.FindElement(ChooseFile));
            Driver.FindElement(ChooseFile).Click();
            
            //Switch to upload window
            Driver.SwitchTo().Window(Driver.WindowHandles.Last());
            //Uploading the file using keyboard actions
            Thread.Sleep(2000);
           
            var fileNamePath = "C:\\Users\\09946103\\Downloads\\single_service.csv";
            SendKeys.SendWait(fileNamePath);
            SendKeys.SendWait(@"{Enter}");
            //Thread.Sleep(2000);
            wait.Until(x => x.FindElement(ConfirmUploadFile_Element));
            //Close the bulk enrolment window
            Driver.FindElement(UploadEnrolments_Element).Click();
            //Brings the control back to original window
            Driver.SwitchTo().Window(winHandleBefore);
        }

        internal void EditEnrolment(string slk)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            wait.Until(x => x.FindElement(Enrolments_Element));
            Driver.FindElement(Enrolments_Element).Click();

            wait.Until(x => x.FindElement(SearchEnrolment_Element));
            Driver.FindElement(SearchEnrolment_Element).Click();
            Driver.FindElement(SearchEnrolment_Element).SendKeys(slk);

            wait.Until(x => x.FindElement(EditEnrolment_Element));
            Driver.FindElement(EditEnrolment_Element).Click();
        }

        internal void EditCommenceDate_Enrolment(string date)
        {
            //Updating the commence date of the educator
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            wait.Until(x => x.FindElement(DateCommenced_Enroll));
            //Clear the date field
            Thread.Sleep(2000);
            Driver.FindElement(DateCommenced_Enroll).Clear();
            Driver.FindElement(DateCommenced_Enroll).SendKeys(date);
            wait.Until(x => x.FindElement(NextBtn_Enroll));
            Driver.FindElement(NextBtn_Enroll).Click();
            //Clicking next button on second page of form
            wait.Until(x => x.FindElement(NextBtn));
            Driver.FindElement(NextBtn).Click();
            wait.Until(x => x.FindElement(SaveButton_Enroll));
            Driver.FindElement(SaveButton_Enroll).Click();
        }

        internal void DeleteEnrolment(string slk)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            wait.Until(x => x.FindElement(Enrolments_Element));
            Driver.FindElement(Enrolments_Element).Click();
            
            wait.Until(x => x.FindElement(SearchEnrolment_Element));
            Driver.FindElement(SearchEnrolment_Element).Click();
            Driver.FindElement(SearchEnrolment_Element).SendKeys(slk);

            wait.Until(x => x.FindElement(DeleteEnrolment_Element));
            Driver.FindElement(DeleteEnrolment_Element).Click();
        }

        internal void Confirm_DeleteEnrolment(string slk)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            wait.Until(x => x.FindElement(SearchEnrolment_Element));
            Driver.FindElement(SearchEnrolment_Element).Click();
            Driver.FindElement(SearchEnrolment_Element).SendKeys(slk);
            bool a = CommonFunctions.IsElementDisplayed(Driver, Element);
            string b = a.ToString();
            Assert.AreEqual(b, "False");
        }
        internal void ConfirmRemove_Enrolment(string reason)
        {
            //Thread.Sleep(2000);
            //Select reason for leaving from dropdown
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            wait.Until(x => x.FindElement(Dropdown_Element));
           //Driver.FindElement(Dropdown_Element).Click();
            var selectElement_reason = new SelectElement(Driver.FindElement(Dropdown_Element));
            selectElement_reason.SelectByText(reason);
            
            wait.Until(x => x.FindElement(FinalDate_ElementIcon));
            //Select final date from date picker
            Driver.FindElement(FinalDate_ElementIcon).Click();
            FinalDate_Select.Click();
            //Click on remove button
            RemoveButton_Enrolment.Click();
            Thread.Sleep(2000);
        }

        internal void FillNewEnrolmentForm(Table table)
        {
            var dataTable = TableExtensions.ToDataTable(table);
            foreach (DataRow row in dataTable.Rows)

            {
                WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
                wait.Until(x => x.FindElement(GivenName_Enroll));
                //random generator of GivenName
                Random generatorGn = new Random();
                int iGn = generatorGn.Next(1, 10000000);
                string sGn = iGn.ToString().PadLeft(7, '0');
                string eGivenName = "EGN." + sGn;
                //removed Driver.FindElement(GivenName_Enroll).SendKeys(row.ItemArray[0].ToString());
                Driver.FindElement(GivenName_Enroll).SendKeys(eGivenName);

                wait.Until(x => x.FindElement(FamilyName_Enroll));
                //random generator of FamilyName
                Random generatorFn = new Random();
                int iFn = generatorFn.Next(1, 10000000);
                string sFn = iFn.ToString().PadLeft(7, '0');
                string eFamilyName = "Fn." + sFn;
                //removed Driver.FindElement(FamilyName_Enroll).SendKeys(row.ItemArray[1].ToString());
                Driver.FindElement(FamilyName_Enroll).SendKeys(eFamilyName);

                wait.Until(x => x.FindElement(Sex_dropdown));
                SelectElement Sex = new SelectElement(Driver.FindElement(Sex_dropdown));
                Sex.SelectByText(row.ItemArray[2].ToString());

                DOB_Enroll.Clear();
                DOB_Enroll.Click();
                //random generator DOB
                DateTime start = new DateTime((DateTime.Now.Year - 5), 1, 1);
                DateTime end = new DateTime((DateTime.Now.Year - 5), 12, 31);
                Random gen = new Random();
                int range = ((TimeSpan)(end - start)).Days;
                string dOB = start.AddDays(gen.Next(range)).ToString("dd/MM/yyyy");
                //removedDOB_Enroll.SendKeys(row.ItemArray[3].ToString());
                DOB_Enroll.SendKeys(dOB);

                wait.Until(x => x.FindElement(UnitNum_Enroll));
                //random generator of Unit number
                Random generatorUn = new Random();
                int iUn = generatorUn.Next(1, 1000000);
                string unitNum = iUn.ToString().PadLeft(6, '0');
                //removed Driver.FindElement(UnitNum_Enroll).SendKeys(row.ItemArray[4].ToString());
                Driver.FindElement(UnitNum_Enroll).SendKeys(unitNum);

                wait.Until(x => x.FindElement(StreetName_Enroll));
                //random generator of Street
                Random generatorStn = new Random();
                int iStn = generatorStn.Next(1, 1000000);
                string sStn = iStn.ToString().PadLeft(6, '0');
                string stnum = "Str-" + DateTime.Now.ToString("ddmmyyyy") + "_" + sStn;
                //Driver.FindElement(StreetName_Enroll).SendKeys(row.ItemArray[5].ToString());
                Driver.FindElement(StreetName_Enroll).SendKeys(stnum);

                var SelectElement_StreetType = new SelectElement(StreetType_Enroll);
                SelectElement_StreetType.SelectByText(row.ItemArray[6].ToString());

                Suburb_Enroll.SendKeys(row.ItemArray[7].ToString());

                var SelectElement_State = new SelectElement(State_Enroll);
                SelectElement_State.SelectByText(row.ItemArray[8].ToString());

                wait.Until(x => x.FindElement(Postcode_Enroll));
                //random generator of Post Code
                Random generatorPc = new Random();
                int iPc = generatorPc.Next(1111, 9999);
                string postCode = iPc.ToString().PadLeft(3, '0');
                //removed Driver.FindElement(Postcode_Enroll).SendKeys(row.ItemArray[9].ToString());
                Driver.FindElement(Postcode_Enroll).SendKeys(postCode);

                wait.Until(x => x.FindElement(DateCommenced_Enroll));
                Driver.FindElement(DateCommenced_Enroll).SendKeys(row.ItemArray[10].ToString());
                wait.Until(x => x.FindElement(NextBtn_Enroll));
                Driver.FindElement(NextBtn_Enroll).Click();
                wait.Until(x => x.FindElement(IndigenousStatus));
                Driver.FindElement(IndigenousStatus).Click();

                var SelectElement_Language = new SelectElement(Language_Element);
                SelectElement_Language.SelectByText(row.ItemArray[11].ToString());


                ChildLive.Click();
                wait.Until(x => x.FindElement(ChildParentRelationship));
                Driver.FindElement(ChildParentRelationship).Click();

                var SelectElement_Immunisation = new SelectElement(Immunisation_Status);
                SelectElement_Immunisation.SelectByText(row.ItemArray[12].ToString());

                var SelectElement_Group = new SelectElement(Group_Attended);
                SelectElement_Group.SelectByText(row.ItemArray[13].ToString());
                EarlyStart_Element.Click();
                Driver.FindElement(NextBtn_Element).Click();
                var SelectElement_AdultAHighest = new SelectElement(AdultA_HighestYear);
                SelectElement_AdultAHighest.SelectByText(row.ItemArray[14].ToString());

                var SelectElement_AdultAQualification = new SelectElement(AdultA_Qualification);
                SelectElement_AdultAQualification.SelectByText(row.ItemArray[15].ToString());

                var SelectElement_AdultAOccupation = new SelectElement(AdultA_Occupation);
                SelectElement_AdultAOccupation.SelectByText(row.ItemArray[16].ToString());

                var SelectElement_AdultBHighest = new SelectElement(AdultB_HighestYear);
                SelectElement_AdultBHighest.SelectByText(row.ItemArray[17].ToString());

                var SelectElement_AdultBQualification = new SelectElement(AdultB_Qualification);
                SelectElement_AdultBQualification.SelectByText(row.ItemArray[18].ToString());

                var SelectElement_AdultBOccupation = new SelectElement(AdultB_Occupation);
                SelectElement_AdultBOccupation.SelectByText(row.ItemArray[19].ToString());
                wait.Until(x => x.FindElement(NextBtn));
                Driver.FindElement(NextBtn).Click();
                wait.Until(x => x.FindElement(SaveButton_Enroll));
                Driver.FindElement(SaveButton_Enroll).Click();
                Thread.Sleep(4000);
                wait.Until(x => x.FindElement(AddNewEnrolment_Element));
                Driver.FindElement(AddNewEnrolment_Element).Click();
            }
        }

        internal void FillNewEnrolmentForm_EskExtn_CP(Table table)
        {
            var dataTable = TableExtensions.ToDataTable(table);
            foreach (DataRow row in dataTable.Rows)

            {
                WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
                wait.Until(x => x.FindElement(GivenName_Enroll));
                //random generator of GivenName
                Random generatorGn = new Random();
                int iGn = generatorGn.Next(1, 10000000);
                string sGn = iGn.ToString().PadLeft(7, '0');
                string eGivenName = "EGN." + sGn;
                //removed Driver.FindElement(GivenName_Enroll).SendKeys(row.ItemArray[0].ToString());
                Driver.FindElement(GivenName_Enroll).SendKeys(eGivenName);

                wait.Until(x => x.FindElement(FamilyName_Enroll));
                //random generator of FamilyName
                Random generatorFn = new Random();
                int iFn = generatorFn.Next(1, 10000000);
                string sFn = iFn.ToString().PadLeft(7, '0');
                string eFamilyName = "Fn." + sFn;
                //removed Driver.FindElement(FamilyName_Enroll).SendKeys(row.ItemArray[1].ToString());
                Driver.FindElement(FamilyName_Enroll).SendKeys(eFamilyName);

                wait.Until(x => x.FindElement(Sex_dropdown));
                SelectElement Sex = new SelectElement(Driver.FindElement(Sex_dropdown));
                Sex.SelectByText(row.ItemArray[2].ToString());

                DOB_Enroll.Clear();
                DOB_Enroll.Click();
                //random generator DOB
                DateTime start = new DateTime((DateTime.Now.Year - 5), 1, 1);
                DateTime end = new DateTime((DateTime.Now.Year - 5), 12, 31);
                Random gen = new Random();
                int range = ((TimeSpan)(end - start)).Days;
                string dOB = start.AddDays(gen.Next(range)).ToString("dd/MM/yyyy");
                //removedDOB_Enroll.SendKeys(row.ItemArray[3].ToString());
                DOB_Enroll.SendKeys(dOB);

                wait.Until(x => x.FindElement(UnitNum_Enroll));
                //random generator of Unit number
                Random generatorUn = new Random();
                int iUn = generatorUn.Next(1, 1000000);
                string unitNum = iUn.ToString().PadLeft(6, '0');
                //removed Driver.FindElement(UnitNum_Enroll).SendKeys(row.ItemArray[4].ToString());
                Driver.FindElement(UnitNum_Enroll).SendKeys(unitNum);

                wait.Until(x => x.FindElement(StreetName_Enroll));
                //random generator of Street
                Random generatorStn = new Random();
                int iStn = generatorStn.Next(1, 1000000);
                string sStn = iStn.ToString().PadLeft(6, '0');
                string stnum = "Str-" + DateTime.Now.ToString("ddmmyyyy") + "_" + sStn;
                //Driver.FindElement(StreetName_Enroll).SendKeys(row.ItemArray[5].ToString());
                Driver.FindElement(StreetName_Enroll).SendKeys(stnum);

                var SelectElement_StreetType = new SelectElement(StreetType_Enroll);
                SelectElement_StreetType.SelectByText(row.ItemArray[6].ToString());

                Suburb_Enroll.SendKeys(row.ItemArray[7].ToString());

                var SelectElement_State = new SelectElement(State_Enroll);
                SelectElement_State.SelectByText(row.ItemArray[8].ToString());

                wait.Until(x => x.FindElement(Postcode_Enroll));
                //random generator of Post Code
                Random generatorPc = new Random();
                int iPc = generatorPc.Next(1111, 9999);
                string postCode = iPc.ToString().PadLeft(3, '0');
                //removed Driver.FindElement(Postcode_Enroll).SendKeys(row.ItemArray[9].ToString());
                Driver.FindElement(Postcode_Enroll).SendKeys(postCode);

                wait.Until(x => x.FindElement(DateCommenced_Enroll));
                Driver.FindElement(DateCommenced_Enroll).SendKeys(row.ItemArray[10].ToString());
                wait.Until(x => x.FindElement(NextBtn_Enroll));
                Driver.FindElement(NextBtn_Enroll).Click();
                wait.Until(x => x.FindElement(IndigenousStatus));
                Driver.FindElement(IndigenousStatus).Click();

                var SelectElement_Language = new SelectElement(Language_Element);
                SelectElement_Language.SelectByText(row.ItemArray[11].ToString());


                ChildLive.Click();
                wait.Until(x => x.FindElement(ChildParentRelationship));
                Driver.FindElement(ChildParentRelationship).Click();

                var SelectElement_Immunisation = new SelectElement(Immunisation_Status);
                SelectElement_Immunisation.SelectByText(row.ItemArray[12].ToString());

                var SelectElement_Group = new SelectElement(Group_Attended);
                SelectElement_Group.SelectByText(row.ItemArray[13].ToString());

                //ESK Extension fields
                EskGrant_Yes_Radio.Click();

                wait.Until(x => x.FindElement(EskGrant_Cp_Radio));
                Driver.FindElement(EskGrant_Cp_Radio).Click();

                wait.Until(x => x.FindElement(EskGrant_Cp_Radio));
                Driver.FindElement(EskGrant_Cp_Radio).Click();

                wait.Until(x => x.FindElement(EskGrant_Program_Radio));
                Driver.FindElement(EskGrant_Program_Radio).Click();

                wait.Until(x => x.FindElement(EskGrant_Apply_Radio));
                Driver.FindElement(EskGrant_Apply_Radio).Click();

                wait.Until(x => x.FindElement(NextBtn_Enroll));
                Driver.FindElement(NextBtn_Enroll).Click();
                //Adult information
                var SelectElement_AdultAHighest = new SelectElement(AdultA_HighestYear);
                SelectElement_AdultAHighest.SelectByText(row.ItemArray[14].ToString());

                var SelectElement_AdultAQualification = new SelectElement(AdultA_Qualification);
                SelectElement_AdultAQualification.SelectByText(row.ItemArray[15].ToString());

                var SelectElement_AdultAOccupation = new SelectElement(AdultA_Occupation);
                SelectElement_AdultAOccupation.SelectByText(row.ItemArray[16].ToString());

                var SelectElement_AdultBHighest = new SelectElement(AdultB_HighestYear);
                SelectElement_AdultBHighest.SelectByText(row.ItemArray[17].ToString());

                var SelectElement_AdultBQualification = new SelectElement(AdultB_Qualification);
                SelectElement_AdultBQualification.SelectByText(row.ItemArray[18].ToString());

                var SelectElement_AdultBOccupation = new SelectElement(AdultB_Occupation);
                SelectElement_AdultBOccupation.SelectByText(row.ItemArray[19].ToString());

                wait.Until(x => x.FindElement(NextBtn));
                Driver.FindElement(NextBtn).Click();

                wait.Until(x => x.FindElement(SaveButton_Enroll));
                Driver.FindElement(SaveButton_Enroll).Click();
                Thread.Sleep(4000);
                wait.Until(x => x.FindElement(AddNewEnrolment_Element));
                Driver.FindElement(AddNewEnrolment_Element).Click();
            }
        }
        internal void FillNewEnrolmentForm_Integrated(Table table)        
        {
            var dataTable = TableExtensions.ToDataTable(table);
            foreach (DataRow row in dataTable.Rows)

            {
                WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
                wait.Until(x => x.FindElement(GivenName_Enroll));
                //random generator of GivenName
                Random generatorGn = new Random();
                int iGn = generatorGn.Next(1, 10000000);
                string sGn = iGn.ToString().PadLeft(7, '0');
                string eGivenName = "EGN." + sGn;
                //removed Driver.FindElement(GivenName_Enroll).SendKeys(row.ItemArray[0].ToString());
                Driver.FindElement(GivenName_Enroll).SendKeys(eGivenName);

                wait.Until(x => x.FindElement(FamilyName_Enroll));
                //random generator of FamilyName
                Random generatorFn = new Random();
                int iFn = generatorFn.Next(1, 10000000);
                string sFn = iFn.ToString().PadLeft(7, '0');
                string eFamilyName = "Fn." + sFn;
                //removed Driver.FindElement(FamilyName_Enroll).SendKeys(row.ItemArray[1].ToString());
                Driver.FindElement(FamilyName_Enroll).SendKeys(eFamilyName);

                wait.Until(x => x.FindElement(Sex_dropdown));
                SelectElement Sex = new SelectElement(Driver.FindElement(Sex_dropdown));
                Sex.SelectByText(row.ItemArray[2].ToString());

                DOB_Enroll.Clear();
                DOB_Enroll.Click();
                //random generator DOB
                DateTime start = new DateTime((DateTime.Now.Year - 5), 1, 1);
                DateTime end = new DateTime((DateTime.Now.Year - 5), 12, 31);
                Random gen = new Random();
                int range = ((TimeSpan)(end - start)).Days;
                string dOB = start.AddDays(gen.Next(range)).ToString("dd/MM/yyyy");
                //removedDOB_Enroll.SendKeys(row.ItemArray[3].ToString());
                DOB_Enroll.SendKeys(dOB);

                wait.Until(x => x.FindElement(UnitNum_Enroll));
                //random generator of Unit number
                Random generatorUn = new Random();
                int iUn = generatorUn.Next(1, 1000000);
                string unitNum = iUn.ToString().PadLeft(6, '0');
                //removed Driver.FindElement(UnitNum_Enroll).SendKeys(row.ItemArray[4].ToString());
                Driver.FindElement(UnitNum_Enroll).SendKeys(unitNum);

                wait.Until(x => x.FindElement(StreetName_Enroll));
                //random generator of Street
                Random generatorStn = new Random();
                int iStn = generatorStn.Next(1, 1000000);
                string sStn = iStn.ToString().PadLeft(6, '0');
                string stnum = "Str-" + DateTime.Now.ToString("ddmmyyyy") + "_" + sStn;
                //Driver.FindElement(StreetName_Enroll).SendKeys(row.ItemArray[5].ToString());
                Driver.FindElement(StreetName_Enroll).SendKeys(stnum);

                var SelectElement_StreetType = new SelectElement(StreetType_Enroll);
                SelectElement_StreetType.SelectByText(row.ItemArray[6].ToString());

                Suburb_Enroll.SendKeys(row.ItemArray[7].ToString());

                var SelectElement_State = new SelectElement(State_Enroll);
                SelectElement_State.SelectByText(row.ItemArray[8].ToString());

                wait.Until(x => x.FindElement(Postcode_Enroll));
                //random generator of Post Code
                Random generatorPc = new Random();
                int iPc = generatorPc.Next(1111, 9999);
                string postCode = iPc.ToString().PadLeft(3, '0');
                //removed Driver.FindElement(Postcode_Enroll).SendKeys(row.ItemArray[9].ToString());
                Driver.FindElement(Postcode_Enroll).SendKeys(postCode);

                wait.Until(x => x.FindElement(DateCommenced_Enroll));
                Driver.FindElement(DateCommenced_Enroll).SendKeys(row.ItemArray[10].ToString());
                wait.Until(x => x.FindElement(NextBtn_Enroll));
                Driver.FindElement(NextBtn_Enroll).Click();
                wait.Until(x => x.FindElement(IndigenousStatus));
                Driver.FindElement(IndigenousStatus).Click();

                var SelectElement_Language = new SelectElement(Language_Element);
                SelectElement_Language.SelectByText(row.ItemArray[11].ToString());


                ChildLive.Click();
                wait.Until(x => x.FindElement(ChildParentRelationship));
                Driver.FindElement(ChildParentRelationship).Click();

                var SelectElement_Immunisation = new SelectElement(Immunisation_Status);
                SelectElement_Immunisation.SelectByText(row.ItemArray[12].ToString());

                
                EarlyStart_Element.Click();
                Driver.FindElement(NextBtn_Element).Click();
                var SelectElement_AdultAHighest = new SelectElement(AdultA_HighestYear);
                SelectElement_AdultAHighest.SelectByText(row.ItemArray[13].ToString());

                var SelectElement_AdultAQualification = new SelectElement(AdultA_Qualification);
                SelectElement_AdultAQualification.SelectByText(row.ItemArray[14].ToString());

                var SelectElement_AdultAOccupation = new SelectElement(AdultA_Occupation);
                SelectElement_AdultAOccupation.SelectByText(row.ItemArray[15].ToString());

                var SelectElement_AdultBHighest = new SelectElement(AdultB_HighestYear);
                SelectElement_AdultBHighest.SelectByText(row.ItemArray[16].ToString());

                var SelectElement_AdultBQualification = new SelectElement(AdultB_Qualification);
                SelectElement_AdultBQualification.SelectByText(row.ItemArray[17].ToString());

                var SelectElement_AdultBOccupation = new SelectElement(AdultB_Occupation);
                SelectElement_AdultBOccupation.SelectByText(row.ItemArray[18].ToString());
                wait.Until(x => x.FindElement(NextBtn));
                Driver.FindElement(NextBtn).Click();
                wait.Until(x => x.FindElement(SaveButton_Enroll));
                Driver.FindElement(SaveButton_Enroll).Click();
                Thread.Sleep(4000);
                wait.Until(x => x.FindElement(AddNewEnrolment_Element));
                Driver.FindElement(AddNewEnrolment_Element).Click();
            }
        }
    }
}