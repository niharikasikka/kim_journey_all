﻿using KIM_SmokeTest.BaseClass;
using KIM_SmokeTest.UtilityClasses;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;
using System;
using System.Data;
using System.Threading;
using TechTalk.SpecFlow;

namespace KIM_Automation.Pages
{
    class Kim2ServicePage : BaseApplicationPage
    {
        public Kim2ServicePage(IWebDriver driver) : base(driver)
        { }
        public By ServiceCategory_Empty => By.XPath("//div[@id='hsd_ecservicecategory']//div[@class='ms-crm-Inline-Value ms-crm-Inline-EmptyValue ms-crm-Inline-Lookup'][contains(text(),'--')]");
        public IWebElement ServiceRegion_SouthWest => Driver.FindElement(By.XPath("//option[contains(text(),'South Western Victoria')]"));
        public By SaveClose_button => By.XPath("//span[contains(text(),'Save & Close')]");
        public By ServiceRegion_Element => By.XPath("//div[@id='hsd_ecserviceregion']//div[@class='ms-crm-Inline-Value ms-crm-Inline-EmptyValue']");
        public IWebElement ServiceCategory_Edit => Driver.FindElement(By.XPath("//input[@id='hsd_ecservicecategory_ledit']"));
        public IWebElement ServiceApprovalId_Empty => Driver.FindElement(By.XPath("//div[@id='hsd_serviceapprovalid']//div[@class='ms-crm-Inline-Value ms-crm-Inline-EmptyValue']"));
        public IWebElement ServiceApprovalId_Edit => Driver.FindElement(By.XPath("//input[@id='hsd_serviceapprovalid_i']"));
        public By ActionElement_Empty => By.CssSelector("#hsd_action");
        public IWebElement ActionElement_Approve => Driver.FindElement(By.XPath("//option[contains(text(),'Approve')]"));
        public IWebElement ServiceLga_Edit => Driver.FindElement(By.XPath("//input[@id='hsd_ecservicelga_ledit']"));
        public IWebElement ServiceLga_Empty => Driver.FindElement(By.XPath("//div[@id='hsd_ecservicelga']//div[@class='ms-crm-Inline-Value ms-crm-Inline-EmptyValue ms-crm-Inline-Lookup'][contains(text(),'--')]"));
        public IWebElement ActionElement_RequestManagerApproval => Driver.FindElement(By.XPath("//option[contains(text(),'Request Manager Approval')]"));
        public By ActionElement_ChangeValue => By.XPath("//span[contains(text(),'Request Manager Approval')]");
        public IWebElement ActionElement_Select => Driver.FindElement(By.CssSelector("#hsd_action_i"));
        public By ECServicer => By.XPath("//div[@id='hsd_ecservice']");
        public IWebElement PreCommitments_Link => Driver.FindElement(By.XPath("//h2[contains(text(),'Pre-Commitments')]"));
        public IWebElement ConfirmStatus_Text => Driver.FindElement(By.XPath("//div[@id='hsd_confirmstatus']//div[@class='ms-crm-Inline-Value ms-crm-Inline-Locked']"));
        public IWebElement Children_Link => Driver.FindElement(By.XPath("//h2[contains(text(),'Children')]"));
        public IWebElement Next_Link => Driver.FindElement(By.XPath("//td[@id='TabGridofChildren_PageInfo']//a[@id='_nextPageImg']"));
        public IWebElement Previous_Link => Driver.FindElement(By.XPath("//td[@id='TabGridofChildren_PageInfo']//img[@id='page_L0']"));
        public IWebElement FirstPage_Link => Driver.FindElement(By.XPath("//td[@id='TabGridofChildren_PageInfo']//img[@id='page_FL0']"));
        public IWebElement Workforce_Link => Driver.FindElement(By.XPath("//a[@name='{cbab201c-8c42-2131-7cdd-b6c3353a0899}']//h2[@class='ms-crm-Form'][contains(text(),'Workforce')]"));
        public IWebElement WorkforceGridView_Link => Driver.FindElement(By.CssSelector("#GridofWorkforce_openAssociatedGridViewImageButton"));
        public IWebElement WFFundingStatus => Driver.FindElement(By.XPath("//div[@id='header_hsd_fundingstatus']//span[contains(text(),'Funded')]"));
        public IWebElement WFLayout => Driver.FindElement(By.XPath("//div[@class='ms-crm-Form-AssociatedGrid-Layout-Lite']"));
        public IWebElement WorkforceSearch_input => Driver.FindElement(By.XPath("//*[@id='crmGrid_hsd_hsd_ecservice_hsd_workforce_ECService_findCriteria']"));

        public By WorkforceSearch_button => By.XPath("//a[@id='crmGrid_hsd_hsd_ecservice_hsd_workforce_ECService_findCriteriaButton']");
        public By WorkforceElement => By.XPath("//div[@class='ms-crm-grid-databodycontainer']//tr[1]//td[3]");
        public IWebElement WFNext_Link => Driver.FindElement(By.XPath("//td[@id='GridofWorkforce_PageInfo']//a[@id='_nextPageImg']"));
        internal void EnterData_NewService(TechTalk.SpecFlow.Table table)
        {
            var dataTable = TableExtensions.ToDataTable(table);
            foreach (DataRow row in dataTable.Rows)
            {

                Driver.SwitchTo().Frame("contentIFrame1");
                Thread.Sleep(7000);
                WebDriverWait waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
                waitAgency.Until(c => c.FindElement(ServiceRegion_Element));
                Driver.FindElement(ServiceRegion_Element).Click();
                Thread.Sleep(3000);
                ServiceRegion_SouthWest.Click();
                Thread.Sleep(3000);
                ServiceApprovalId_Empty.Click();

                //random generator of Service Approval ID
                Random generator = new Random();
                int r = generator.Next(1, 100000000);
                string s = r.ToString().PadLeft(8, '0');
                string serviceApprovalID = "SR-" + s;
                ServiceApprovalId_Edit.SendKeys(serviceApprovalID);
                //ServiceApprovalId_Edit.SendKeys(row.ItemArray[3].ToString());

                Thread.Sleep(3000);
                ServiceLga_Empty.Click();
                ServiceLga_Edit.SendKeys(row.ItemArray[2].ToString());
                Thread.Sleep(3000);
                waitAgency.Until(c => c.FindElement(ServiceCategory_Empty));
                Driver.FindElement(ServiceCategory_Empty).Click();
                ServiceCategory_Edit.SendKeys(row.ItemArray[1].ToString());
                Thread.Sleep(3000);
                waitAgency.Until(c => c.FindElement(ActionElement_Empty));
                Driver.FindElement(ActionElement_Empty).Click();
                ActionElement_RequestManagerApproval.Click();
                //Save and close the form
                Driver.SwitchTo().DefaultContent();
                Thread.Sleep(3000);
                waitAgency.Until(c => c.FindElement(Save_button));
                Driver.FindElement(Save_button).Click();
                Thread.Sleep(15000);
            }
        }

        internal void ApproveService(string status)
        {
            Driver.SwitchTo().Frame("contentIFrame1");
            WebDriverWait waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            waitAgency.Until(c => c.FindElement(ActionElement_Empty));
            Thread.Sleep(12000);
            Driver.FindElement(ActionElement_Empty).Click();
            Thread.Sleep(2000);
            var selectElement = new SelectElement(ActionElement_Select);
            selectElement.SelectByValue("864710006");
            Driver.SwitchTo().DefaultContent();
            waitAgency.Until(c => c.FindElement(Save_button));
            Driver.FindElement(Save_button).Click();
            Thread.Sleep(18000);
        }

        internal void Verify_FundingStatus(string status)
        {
            Thread.Sleep(6000);
            Driver.SwitchTo().Frame("contentIFrame1");
            Thread.Sleep(2000);
            Assert.IsTrue(ConfirmStatus_Text.Text.Equals(status));
            Driver.SwitchTo().DefaultContent();
        }

        internal void Verify_PreCommitments(Table table)
        {
            string Units_number = "0";
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(50);
            Thread.Sleep(5000);
            Driver.SwitchTo().Frame("contentIFrame1");
            Thread.Sleep(5000);
            PreCommitments_Link.Click();
            Thread.Sleep(5000);
            var dataTable = TableExtensions.ToDataTable(table);
            foreach (DataRow row in dataTable.Rows)
            {

                var TableRows = Driver.FindElements(By.XPath("//div[@id='tab8']//div[@class='ms-crm-grid-databodycontainer-Ex']//tbody//tr"));

                foreach (var tableRow in TableRows)
                {
                    var td = tableRow.FindElements(By.TagName("td"));
                    if (td[6].Text.Contains(row.ItemArray[0].ToString()))
                    {
                        Units_number = td[9].FindElement(By.TagName("div")).Text;
                        break;
                    }

                }
                Assert.IsTrue(Units_number.Equals(row.ItemArray[1].ToString()));

            }
            Driver.SwitchTo().DefaultContent();

        }
        public IWebElement EYM_StartDate => Driver.FindElement(By.XPath("//div[@id='hsd_clusterstartdate']//div[contains(@class,'ms-crm-Inline-Value')]"));
        public IWebElement EYMFunding_StartDate => Driver.FindElement(By.XPath("//div[@id='hsd_clusterfundingstartdate']//div[contains(@class,'ms-crm-Inline-Value')]"));
        public IWebElement EYMFunding_EndDate => Driver.FindElement(By.XPath("//div[@id='hsd_clusterfundingenddate']//div[contains(@class,'ms-crm-Inline-Value')]"));
        public IWebElement Notes_Section => Driver.FindElement(By.XPath("//h2[contains(text(),'Notes')]"));
        public IWebElement Notes_TextArea => Driver.FindElement(By.XPath("//textarea[@id='createNote_notesTextBox']"));
        public IWebElement EYM_ManagedService => Driver.FindElement(By.XPath("//div[@id='hsd_clustermanagedservice']//div[@class='ms-crm-Inline-Value']"));
        public IWebElement EYM_Section => Driver.FindElement(By.XPath("//h2[contains(text(),'Early Years Management')]"));
        public IWebElement Done_Button => Driver.FindElement(By.XPath("//button[@id='postButton']"));
        public IWebElement ActionElement_RequestCentralApproval => Driver.FindElement(By.XPath("//option[contains(text(),'Request Central Office Approval')]"));
        public IWebElement ActionElement_COApprove => Driver.FindElement(By.XPath("//option[contains(text(),'Approve')]"));
        public By Save_button => By.XPath("//li[@id='hsd_ecservice|NoRelationship|Form|Mscrm.Form.hsd_ecservice.Save']//a[@class='ms-crm-Menu-Label']");
        public By Delete_button => By.XPath("//li[@id='hsd_ecservice|NoRelationship|Form|Mscrm.Form.hsd_ecservice.Delete']");
        public By DeleteConfirm_button => By.XPath("//button[@id='butBegin']");
        public By SaveChild_Button => By.XPath("//li[@id='hsd_ecservice|NoRelationship|Form|Mscrm.Form.hsd_ecservice.Save']//span[@class='ms-crm-CommandBar-Menu'][contains(text(),'Save')]");
        public IWebElement EYM_StartDate_Input => Driver.FindElement(By.XPath("//table[@id='hsd_clusterstartdate_i']//input[@id='DateInput']"));
        public IWebElement EYMFunding_StartDate_Input => Driver.FindElement(By.XPath("//table[@id='hsd_clusterfundingstartdate_i']//input[@id='DateInput']"));
        public IWebElement EYMFunding_EndDate_Input => Driver.FindElement(By.XPath("//table[@id='hsd_clusterfundingenddate_i']//input[@id='DateInput']"));

        internal void FillForm_Acs1(Table table)
        {
            Thread.Sleep(10000);
            WebDriverWait waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            Driver.SwitchTo().Frame("contentIFrame1");
            ((IJavaScriptExecutor)Driver).ExecuteScript("arguments[0].scrollIntoView(true);", EYM_Section);
            Thread.Sleep(10000);
            EYM_ManagedService.Click();
            var dataTable = TableExtensions.ToDataTable(table);
            foreach (DataRow row in dataTable.Rows)
            {
                Thread.Sleep(3000);
                EYM_StartDate.Click();
                EYM_StartDate_Input.SendKeys(row.ItemArray[0].ToString());
                Thread.Sleep(3000);
                EYMFunding_StartDate.Click();
                EYMFunding_StartDate_Input.SendKeys(row.ItemArray[1].ToString());
                Thread.Sleep(3000);
                EYMFunding_EndDate.Click();
                EYMFunding_EndDate_Input.SendKeys(row.ItemArray[2].ToString());
                Thread.Sleep(3000);
                Notes_Section.Click();
                Notes_TextArea.SendKeys(row.ItemArray[1].ToString());
                Thread.Sleep(3000);
                Done_Button.Click();
                Thread.Sleep(3000);
                ((IJavaScriptExecutor)Driver).ExecuteScript("arguments[0].scrollIntoView(true);", Driver.FindElement(ActionElement_Empty));
                waitAgency.Until(c => c.FindElement(ActionElement_Empty));
                Driver.FindElement(ActionElement_Empty).Click();
                ActionElement_RequestCentralApproval.Click();
                Thread.Sleep(3000);
            }
            Driver.SwitchTo().DefaultContent();
            Thread.Sleep(4000);
            waitAgency.Until(c => c.FindElement(Save_button));
            Driver.FindElement(Save_button).Click();
            Thread.Sleep(20000);
        }
        internal void FillForm_Acs1_WithoutEymStartDate(Table table)
        {
            Thread.Sleep(5000);
            WebDriverWait waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            Driver.SwitchTo().Frame("contentIFrame1");
            ((IJavaScriptExecutor)Driver).ExecuteScript("arguments[0].scrollIntoView(true);", EYM_Section);
            Thread.Sleep(3000);
            EYM_ManagedService.Click();
            var dataTable = TableExtensions.ToDataTable(table);
            foreach (DataRow row in dataTable.Rows)
            {
                EYM_StartDate.Click();
                //EYM_StartDate_Input.SendKeys(row.ItemArray[0].ToString());
                EYMFunding_StartDate.Click();
                EYMFunding_StartDate_Input.SendKeys(row.ItemArray[1].ToString());
                EYMFunding_EndDate.Click();
                EYMFunding_EndDate_Input.SendKeys(row.ItemArray[2].ToString());
                Thread.Sleep(2000);
                Notes_Section.Click();
                Notes_TextArea.SendKeys(row.ItemArray[1].ToString());
                Done_Button.Click();
                Thread.Sleep(3000);
                ((IJavaScriptExecutor)Driver).ExecuteScript("arguments[0].scrollIntoView(true);", Driver.FindElement(ActionElement_Empty));
                waitAgency.Until(c => c.FindElement(ActionElement_Empty));
                Driver.FindElement(ActionElement_Empty).Click();
                ActionElement_RequestCentralApproval.Click();
            }
            Driver.SwitchTo().DefaultContent();
            waitAgency.Until(c => c.FindElement(SaveClose_button));
            Driver.FindElement(SaveClose_button).Click();
        }

        internal void FillForm_CSK4()
        {
            Thread.Sleep(5000);
            WebDriverWait waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            Driver.SwitchTo().Frame("contentIFrame1");
            ((IJavaScriptExecutor)Driver).ExecuteScript("arguments[0].scrollIntoView(true);", EYM_Section);
            Thread.Sleep(4000);
            //Make eym managed service as no
            EYM_ManagedService.Click();
            Thread.Sleep(3000);
            //Set Eym end date as current date
            EYM_EndDate.Click();
            EYM_EndDate_Input.Clear();
            EYM_EndDate_Input.SendKeys(DateTime.Now.ToString("d/MM/yyyy"));
            Thread.Sleep(3000);
            //Enter eym funding end date as current date
            EYMFunding_EndDate.Click();
            EYMFunding_EndDate_Input.Clear();
            EYMFunding_EndDate_Input.SendKeys(DateTime.Now.ToString("d/MM/yyyy"));
            Thread.Sleep(3000);
            //Move the window view to action element
            ((IJavaScriptExecutor)Driver).ExecuteScript("arguments[0].scrollIntoView(true);", Driver.FindElement(ActionElement_Empty));
            waitAgency.Until(c => c.FindElement(ActionElement_Empty));
            Driver.FindElement(ActionElement_Empty).Click();
            ActionElement_RequestCentralApproval.Click();
            //Save and close the form
            Driver.SwitchTo().DefaultContent();
            waitAgency.Until(c => c.FindElement(Save_button));
            Driver.FindElement(Save_button).Click();
            Thread.Sleep(20000);
        }

        internal void Status_Approve()
        {
            WebDriverWait waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            Thread.Sleep(3000);
            Driver.SwitchTo().Frame("contentIFrame1");
            waitAgency.Until(c => c.FindElement(ActionElement_Empty));
            Driver.FindElement(ActionElement_Empty).Click();
            Thread.Sleep(2000);
            ActionElement_COApprove.Click();
            Thread.Sleep(20000);
            Driver.SwitchTo().DefaultContent();
            waitAgency.Until(c => c.FindElement(Save_button));
            Driver.FindElement(Save_button).Click();
            Thread.Sleep(20000);
        }
        internal void Approve_PreCommitments(Table table)
        {

            
            Thread.Sleep(2000);
            Driver.SwitchTo().Frame("contentIFrame1");
            Thread.Sleep(2000);
            PreCommitments_Link.Click();
            Thread.Sleep(2000);
            var dataTable = TableExtensions.ToDataTable(table);
            foreach (DataRow row in dataTable.Rows)
            {
                var TableRows = Driver.FindElements(By.XPath("//div[@id='tab8']//div[@class='ms-crm-grid-databodycontainer-Ex']//tbody//tr"));

                foreach (var tableRow in TableRows)
                {
                    var td = tableRow.FindElements(By.TagName("td"));
                    if (td[6].Text.Contains(row.ItemArray[0].ToString()))
                    {
                        Actions actions = new Actions(Driver);
                        actions.DoubleClick(td[5].FindElement(By.TagName("nobr"))).Perform();

                        WebDriverWait waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
                        waitAgency.Until(c => c.FindElement(ActionElement_Empty));
                        Thread.Sleep(2000);
                        Driver.FindElement(ActionElement_Empty).Click();
                        Thread.Sleep(2000);
                        var selectElement = new SelectElement(ActionElement_Select);
                        selectElement.SelectByValue(row.ItemArray[1].ToString());

                        //Save and close the form
                        Driver.SwitchTo().DefaultContent();
                        Thread.Sleep(2000);
                        waitAgency.Until(c => c.FindElement(SaveClose_button));
                        Driver.FindElement(SaveClose_button).Click();

                        Thread.Sleep(2000);
                        Driver.SwitchTo().Frame("contentIFrame1");
                        Thread.Sleep(2000);
                        PreCommitments_Link.Click();
                        Thread.Sleep(2000);
                        //var dataTable = TableExtensions.ToDataTable(table);
                        break;
                        
                    }
                    
                }
            }
            Driver.SwitchTo().DefaultContent();

            //Save and close the form
            WebDriverWait waitAgency2 = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            Driver.SwitchTo().DefaultContent();
            Thread.Sleep(2000);
            waitAgency2.Until(c => c.FindElement(SaveClose_button));
            Driver.FindElement(SaveClose_button).Click();

        }
        internal void Verify_ConfirmedEnrolment_Children(Table table)
        {
            string Status = "0";
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(50);
            Thread.Sleep(2000);
            Driver.SwitchTo().Frame("contentIFrame1");
            Thread.Sleep(2000);
            Children_Link.Click();
            Thread.Sleep(2000);
            var dataTable = TableExtensions.ToDataTable(table);
            foreach (DataRow row in dataTable.Rows)
            {
                // reset status
                Status = "";

                //flag that next is clicked
                bool isNextClicked = false;

                do
                {
                    var TableRows = Driver.FindElements(By.XPath("//div[@id='TabGridofChildren_divDataArea']//div//tbody//tr"));
                    foreach (var tableRow in TableRows)
                    {
                        var td = tableRow.FindElements(By.TagName("td"));
                        if (td[1].Text.Contains(row.ItemArray[0].ToString()))
                        {
                            Status = td[6].FindElement(By.TagName("nobr")).Text;
                            break;
                        }
                    }
                    //check if SLK is found by checking status
                    if (Status == "")
                    {
                        //click next code; then set flag to true
                        Next_Link.Click();
                        isNextClicked = true;
                    }
                    else
                    {
                        FirstPage_Link.Click();
                        //set to false to break the do while
                        isNextClicked = false;
                    }
                } while (isNextClicked == true);

                //assert status
                Assert.IsTrue(Status.Equals(row.ItemArray[1].ToString()), "Test: " + Status + " should be " + row.ItemArray[1].ToString());
            }
            Driver.SwitchTo().DefaultContent();
        }

        public IWebElement General_Section => Driver.FindElement(By.XPath("//h2[contains(text(),'General')]"));
        public IWebElement EYM_EndDate => Driver.FindElement(By.XPath("//div[@id='hsd_clusterenddate']//div[contains(@class,'ms-crm-Inline-Value ms-crm-Inline')]"));
        public IWebElement EYM_EndDate_Input => Driver.FindElement(By.XPath("//table[@id='hsd_clusterenddate_i']//input[@id='DateInput']"));
        public By Save_Button_Precommitments => By.XPath("//li[@id='hsd_precommitment|NoRelationship|Form|Mscrm.Form.hsd_precommitment.Save']//a[@class='ms-crm-Menu-Label']");

        public IWebElement Precommit_Note_Link => Driver.FindElement(By.XPath("//h2[contains(text(),'Notes')]"));

        internal void AddNote(string note)
        {
            Thread.Sleep(2000);
            WebDriverWait waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            Driver.SwitchTo().Frame("contentIFrame1");
            ((IJavaScriptExecutor)Driver).ExecuteScript("arguments[0].scrollIntoView(true);", Notes_Section);
            Thread.Sleep(2000);
            Notes_Section.Click();
            Notes_TextArea.SendKeys(note);
            Thread.Sleep(3000);
            ((IJavaScriptExecutor)Driver).ExecuteScript("arguments[0].scrollIntoView(true);", Done_Button);
            Done_Button.Click();
            Thread.Sleep(2000);
            ((IJavaScriptExecutor)Driver).ExecuteScript("arguments[0].scrollIntoView(true);", General_Section);
            waitAgency.Until(c => c.FindElement(ActionElement_Empty));
            Driver.FindElement(ActionElement_Empty).Click();
            ActionElement_RequestManagerApproval.Click();
            Driver.SwitchTo().DefaultContent();
            waitAgency.Until(c => c.FindElement(Save_button));
            Driver.FindElement(Save_button).Click();
            Thread.Sleep(20000);
        }

        internal void NavigateEnrolment(Table table)
        {
            Thread.Sleep(2000);
            WebDriverWait waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            Driver.SwitchTo().Frame("contentIFrame1");
            ((IJavaScriptExecutor)Driver).ExecuteScript("arguments[0].scrollIntoView(true);", Children_Link);
            Thread.Sleep(1000);
            Children_Link.Click();
            Thread.Sleep(2000);
            var dataTable = TableExtensions.ToDataTable(table);

            foreach (DataRow row in dataTable.Rows)
            {
                var TableRows = Driver.FindElements(By.XPath("//div[@id='tab9']//div[@class='ms-crm-grid-databodycontainer-Ex']//tbody//tr"));

                foreach (var tableRow in TableRows)
                {
                    var td = tableRow.FindElements(By.TagName("td"));
                    if (td[1].Text.Contains(row.ItemArray[0].ToString()))
                    {
                        Thread.Sleep(2000);
                        Actions actions = new Actions(Driver);
                        //actions.DoubleClick(td[1].FindElement(By.XPath("//div[contains(text(),'" + td[1].Text + "')]")));
                        actions.DoubleClick(td[1].FindElement(By.TagName("div"))).Perform();
                        Thread.Sleep(2000);
                        break;
                    }
                }
                Thread.Sleep(2000);
                ((IJavaScriptExecutor)Driver).ExecuteScript("arguments[0].scrollIntoView(true);", Notes_Section);
                Notes_Section.Click();
                Notes_TextArea.SendKeys(row.ItemArray[1].ToString());
                Thread.Sleep(2000);
                ((IJavaScriptExecutor)Driver).ExecuteScript("arguments[0].click();", Done_Button);
                //Done_Button.Click();
                Thread.Sleep(20000);
            }
            ((IJavaScriptExecutor)Driver).ExecuteScript("arguments[0].scrollIntoView(true);", General_Section);
            Driver.SwitchTo().DefaultContent();
            Thread.Sleep(5000);
            //waitAgency.Until(c => c.FindElement(Save_button));
            //Driver.FindElement(Save_button).Click();
        }
        internal void Verify_Workforce(int eRNoofTeacher, int eRNoofEducator)
        {
            WebDriverWait waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(50);
            Thread.Sleep(2000);
            Driver.SwitchTo().Frame("contentIFrame1");
            Thread.Sleep(2000);
            Workforce_Link.Click();
            Thread.Sleep(2000);

            waitAgency.Until(c => c.FindElements(By.XPath("//div[@id='GridofWorkforce_divDataArea']//tbody")));
            int tableRowsCount = 0;

            //flag that next is clicked
            bool isNextClicked = false;

            do
            {
                tableRowsCount = Driver.FindElements(By.XPath("//div[@id='GridofWorkforce_divDataArea']//div//tbody//tr")).Count + tableRowsCount;
                
                if (WFNext_Link.GetAttribute("style").Equals("cursor: default;"))
                {
                    isNextClicked = false;
                }
                else
                {
                    WFNext_Link.Click();
                    isNextClicked = true; 
                }
                
            }
            while (isNextClicked == true);
            

            Assert.AreEqual((eRNoofTeacher + eRNoofEducator), tableRowsCount, "failed");

            Driver.SwitchTo().DefaultContent();
        }
        internal void WorkforceParentalLeave(Table table)
        {

            int fundingType = 0;
            WebDriverWait waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(50);
            Thread.Sleep(2000);
            Driver.SwitchTo().Frame("contentIFrame1");
            Thread.Sleep(2000);
            //Assert.IsTrue(WFFundingStatus.Displayed);
            Workforce_Link.Click();
            Thread.Sleep(2000);
            WorkforceGridView_Link.Click();
            Thread.Sleep(2000);
            //Driver.SwitchTo().Frame("contentIFrame2");



            var dataTable = TableExtensions.ToDataTable(table);
            foreach (DataRow row in dataTable.Rows)
            {
                Driver.SwitchTo().Frame("area_hsd_hsd_ecservice_hsd_workforce_ECServiceFrame");
                WorkforceSearch_input.Click();
                WorkforceSearch_input.SendKeys(row.ItemArray[0].ToString());

                Thread.Sleep(3000);
                waitAgency.Until(c => c.FindElement(WorkforceSearch_button));
                Driver.FindElement(WorkforceSearch_button).Click();

                Thread.Sleep(2000);
                Actions actions = new Actions(Driver);
                actions.DoubleClick(Driver.FindElement(By.XPath("//div[@class='ms-crm-grid-databodycontainer']//tr[1]//td[3]"))).Perform();

                Thread.Sleep(2000);
                Driver.SwitchTo().Frame("contentIFrame1");


                var TableRows = Driver.FindElements(By.XPath("//table[@id='gridBodyTable']//tbody//tr"));
                foreach (var tableRow in TableRows)
                {
                    var td = tableRow.FindElements(By.TagName("td"));

                    if (td[2].Text.Contains(row.ItemArray[1].ToString()))
                    {
                        if (td[9].Text.Contains(row.ItemArray[3].ToString()))
                        {
                            fundingType = fundingType + 1;
                        }
                        
                    }
                }
                Assert.AreEqual(1, fundingType, "Parental leave is not in KIM2");

                Thread.Sleep(2000);
                Actions actions2 = new Actions(Driver);
                actions2.DoubleClick(Driver.FindElement(By.XPath("//div[@class='ms-crm-grid-databodycontainer-Ex']//td[3]"))).Perform();

                //Thread.Sleep(2000);
                //Driver.SwitchTo().DefaultContent();

                //Thread.Sleep(2000);
                //Driver.SwitchTo().Frame("contentIFrame0");

                waitAgency.Until(c => c.FindElement(ActionElement_Empty));
                Thread.Sleep(2000);
                Driver.FindElement(ActionElement_Empty).Click();
                Thread.Sleep(2000);
                var selectElement = new SelectElement(ActionElement_Select);
                selectElement.SelectByValue(row.ItemArray[2].ToString());

                //Save and close the form
                Driver.SwitchTo().DefaultContent();
                Thread.Sleep(2000);
                waitAgency.Until(c => c.FindElement(SaveClose_button));
                Driver.FindElement(SaveClose_button).Click();
            }

            Driver.SwitchTo().DefaultContent();
        }
        internal void WorkforceParentalLeaveApproval(Table table)
        {

            int fundingType = 0;
            WebDriverWait waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(50);
            Thread.Sleep(2000);
            Driver.SwitchTo().Frame("contentIFrame1");
            Thread.Sleep(2000);
            //Assert.IsTrue(WFFundingStatus.Displayed);
            Workforce_Link.Click();
            Thread.Sleep(2000);
            WorkforceGridView_Link.Click();
            Thread.Sleep(2000);
            //Driver.SwitchTo().Frame("contentIFrame2");



            var dataTable = TableExtensions.ToDataTable(table);
            foreach (DataRow row in dataTable.Rows)
            {
                Driver.SwitchTo().Frame("area_hsd_hsd_ecservice_hsd_workforce_ECServiceFrame");
                WorkforceSearch_input.Click();
                WorkforceSearch_input.SendKeys(row.ItemArray[0].ToString());

                Thread.Sleep(3000);
                waitAgency.Until(c => c.FindElement(WorkforceSearch_button));
                Driver.FindElement(WorkforceSearch_button).Click();

                Thread.Sleep(2000);
                Actions actions = new Actions(Driver);
                actions.DoubleClick(Driver.FindElement(By.XPath("//div[@class='ms-crm-grid-databodycontainer']//tr[1]//td[3]"))).Perform();

                Thread.Sleep(2000);
                Driver.SwitchTo().Frame("contentIFrame1");


                var TableRows = Driver.FindElements(By.XPath("//table[@id='gridBodyTable']//tbody//tr"));
                foreach (var tableRow in TableRows)
                {
                    var td = tableRow.FindElements(By.TagName("td"));

                    if (td[2].Text.Contains(row.ItemArray[1].ToString()))
                    {
                        fundingType = fundingType + 1;


                    }
                }
                Assert.AreEqual(1, fundingType, "Parental leave is not in KIM2");

                Thread.Sleep(2000);
                Actions actions2 = new Actions(Driver);
                actions2.DoubleClick(Driver.FindElement(By.XPath("//div[@class='ms-crm-grid-databodycontainer-Ex']//td[3]"))).Perform();

                Thread.Sleep(2000);
                Driver.SwitchTo().Frame("contentIFrame1");

                waitAgency.Until(c => c.FindElement(ActionElement_Empty));
                Thread.Sleep(2000);
                Driver.FindElement(ActionElement_Empty).Click();
                Thread.Sleep(2000);
                var selectElement = new SelectElement(ActionElement_Select);
                selectElement.SelectByValue(row.ItemArray[2].ToString());

                //Save and close the form
                Driver.SwitchTo().DefaultContent();
                Thread.Sleep(2000);
                waitAgency.Until(c => c.FindElement(SaveClose_button));
                Driver.FindElement(SaveClose_button).Click();
            }

            Driver.SwitchTo().DefaultContent();
        }

        internal void Approve_PreCommitments_withTasks(Table table, string task, string note)
        {
           // Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(50);
            //Thread.Sleep(2000);
            Driver.SwitchTo().Frame("contentIFrame1");
            //Go down to pre commitment link
            ((IJavaScriptExecutor)Driver).ExecuteScript("arguments[0].scrollIntoView(true);", PreCommitments_Link);
            PreCommitments_Link.Click();
            Thread.Sleep(2000);
            var dataTable = TableExtensions.ToDataTable(table);
            foreach (DataRow row in dataTable.Rows)
            {
                var TableRows = Driver.FindElements(By.XPath("//div[@id='tab8']//div[@class='ms-crm-grid-databodycontainer-Ex']//tbody//tr"));

                foreach (var tableRow in TableRows)
                {
                    var td = tableRow.FindElements(By.TagName("td"));
                    try
                    {
                        if (td[6].Text.Contains(row.ItemArray[0].ToString()) & td[5].Text.Equals("Waiting Region Approval"))
                        {
                            Thread.Sleep(4000);
                            Actions actions = new Actions(Driver);
                            actions.DoubleClick(td[5].FindElement(By.TagName("nobr"))).Perform();

                            WebDriverWait waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
                            Thread.Sleep(5000);
                            waitAgency.Until(c => c.FindElement(ActionElement_Empty));
                            //Thread.Sleep(2000);
                            Driver.FindElement(ActionElement_Empty).Click();
                            //Thread.Sleep(2000);
                            var selectElement = new SelectElement(ActionElement_Select);
                            selectElement.SelectByText("Approve");
                            //Adding a note
                            ((IJavaScriptExecutor)Driver).ExecuteScript("arguments[0].scrollIntoView(true);", Precommit_Note_Link);
                            //Thread.Sleep(2000);
                            Notes_Section.Click();
                            Notes_TextArea.SendKeys(note);
                            Done_Button.Click();
                            //Thread.Sleep(3000);

                            Thread.Sleep(2000);
                            //Save the form
                            Driver.SwitchTo().DefaultContent();
                            Thread.Sleep(2000);
                            waitAgency.Until(c => c.FindElement(Save_Button_Precommitments));
                            Driver.FindElement(Save_Button_Precommitments).Click();
                            Thread.Sleep(10000);
                            //Navigate to annual confirmation task
                            Kim2HomePage kim2Home = new Kim2HomePage(Driver);
                            kim2Home.NavigateToTask_PreCommitment();
                            kim2Home.TaskComplete(task);
                            //Save and close the form
                            //Driver.SwitchTo().DefaultContent();
                            Thread.Sleep(2000);
                            waitAgency.Until(c => c.FindElement(SaveClose_button));
                            Driver.FindElement(SaveClose_button).Click();
                            //Go back to precommitments link
                            Driver.SwitchTo().Frame("contentIFrame1");
                            Thread.Sleep(2000);
                            ((IJavaScriptExecutor)Driver).ExecuteScript("arguments[0].scrollIntoView(true);", PreCommitments_Link);
                            PreCommitments_Link.Click();
                            Thread.Sleep(2000);
                            //var dataTable = TableExtensions.ToDataTable(table);
                            break;
                        }
                    }
                    catch (EvaluateException)
                    {
                        
                    }
                }
            }
            Driver.SwitchTo().DefaultContent();

            //Save and close the form
            WebDriverWait waitAgency2 = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            Driver.SwitchTo().DefaultContent();
            Thread.Sleep(2000);
            waitAgency2.Until(c => c.FindElement(SaveClose_button));
            Driver.FindElement(SaveClose_button).Click();
        }

        internal void WorkforceTravelAllowance_ServiceMissingInformation(Table table)
        {

            int fundingType = 0;
            WebDriverWait waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(50);
            Thread.Sleep(2000);
            Driver.SwitchTo().Frame("contentIFrame1");
            Thread.Sleep(2000);
            //Assert.IsTrue(WFFundingStatus.Displayed);
            Workforce_Link.Click();
            Thread.Sleep(2000);
            WorkforceGridView_Link.Click();
            Thread.Sleep(2000);
            //Driver.SwitchTo().Frame("contentIFrame2");



            var dataTable = TableExtensions.ToDataTable(table);
            foreach (DataRow row in dataTable.Rows)
            {
                Driver.SwitchTo().Frame("area_hsd_hsd_ecservice_hsd_workforce_ECServiceFrame");
                WorkforceSearch_input.Click();
                WorkforceSearch_input.SendKeys(row.ItemArray[0].ToString());

                Thread.Sleep(3000);
                waitAgency.Until(c => c.FindElement(WorkforceSearch_button));
                Driver.FindElement(WorkforceSearch_button).Click();

                Thread.Sleep(2000);
                Actions actions = new Actions(Driver);
                actions.DoubleClick(Driver.FindElement(By.XPath("//div[@class='ms-crm-grid-databodycontainer']//tr[1]//td[3]"))).Perform();

                Thread.Sleep(2000);
                Driver.SwitchTo().Frame("contentIFrame1");


                var TableRows = Driver.FindElements(By.XPath("//table[@id='gridBodyTable']//tbody//tr"));
                foreach (var tableRow in TableRows)
                {
                    var td = tableRow.FindElements(By.TagName("td"));
                    int tableRowsCount = TableRows.Count;
                    if (tableRowsCount > 1)
                    {
                        if (td[2].Text.Contains(row.ItemArray[1].ToString()))
                        {
                            if (td[9].Text.Contains(row.ItemArray[3].ToString()))
                            {
                                fundingType = fundingType + 1;
                            }

                        }
                    }

                    
                }
                Assert.AreEqual(0, fundingType, "Travel Allowance was incorrectly created");
                                
            }
            Driver.Navigate().Back();
            Driver.SwitchTo().DefaultContent();
        }
        internal void Delete_Service()
        {
            WebDriverWait waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            Thread.Sleep(20000);
            Driver.SwitchTo().DefaultContent();
            //click delete from the menu
            waitAgency.Until(c => c.FindElement(Delete_button));
            Driver.FindElement(Delete_button).Click();
            Thread.Sleep(20000);

            //click delete button from the Confirm Deletion
            Driver.SwitchTo().Frame("InlineDialog_Iframe");
            waitAgency.Until(c => c.FindElement(DeleteConfirm_button));
            Driver.FindElement(DeleteConfirm_button).Click();
            Thread.Sleep(20000);
        }
    }
}