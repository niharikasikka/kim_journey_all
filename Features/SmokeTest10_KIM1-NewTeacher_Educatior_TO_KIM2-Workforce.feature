﻿Feature: SmokeTest10_KIM1-NewTeacher_Educatior_TO_KIM2-Workforce
##########################################################################################################################################################################################################################################################################################################################################################################################################
## This test automation is to verify the following
## (1) able to add teacher
## (2) able to add educator
## (3) # of teacher and co-educator in KIM1 are the same as Workforce in KIM2
##
## THINGS TO DO/NOTE:
## Existing service "NCL SP110719_80256" to be updated below is under "Moonee Valley City Council" service provider which is setup in Resource.resx > ServiceProvider
## Modify Teacher Given Name, Family Name and VITNumber
## Modify Educator Given Name and Family Name
## Build/Rebuild
##########################################################################################################################################################################################################################################################################################################################################################################################################

@smoke
Scenario Outline: 01 AddTeacher in KIM1
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I add teachers
	| Given Name          | Family Name         | Gender | Date of Birth  | CommencementDate | RegistrationStatus       | VITNumber  | University      | Course                          | Year | Groups | TeacherShare | IndustrialAgreement                             | Level | hours |
	| Teacher110719_80552 | William110719_80552 | Female | " 02/05/1988 " | " 12/07/2019 "   | Fully registered teacher | 1107190529 | Bond University | Bachelor of Children's Services | 2008 | 5      | No           | Early Education Employees Agreement 2016 (EEEA) | 1.1   | 5     |
	And I close the browser successfully
	Examples:
	| service            |
	| NCL SP110719_80256 |

@smoke
Scenario Outline: 02 Add Educator in KIM1
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I add educators
	| Given Name           | Family Name        | Gender | Date of Birth  | HighestTeachingQualification | IndustrialAgreement                          | Level | ProfDevHours | HoursofEmployment | HoursWorkedinAnyprogram |
	| Educator110719_80552 | Hafner110719_80552 | Male   | " 01/10/1988 " | Kindercraft Assistant        | A local council agreement with EEEA appended | 1.1   | 80           | 40                | 50                      |

	And I close the browser successfully
	Examples:
	| service            |
	| NCL SP110719_80256 |

@smoke
Scenario: 03 Run YBSK wrapper job
	Given I go to manage job url
	When I run the ybsk wrapper job to import programs, child etc to kim crm
	Then the service is successfully run
	And I logout of manage jobs
	And I close the browser successfully

###Wait for specified miliseconds
Scenario Outline: 04 WaitScript
    Given I wait for <few> mins
Examples: 
	| few    |      
	| 300000 |

@smoke
Scenario Outline: 05 Verify update in KIM2Workforce
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I check the number of teacher in Teachers tab
	And I check the number of educator in Other Educators tab
	And I close the browser successfully	
	Given I login to Kim crm application as system admin 
	When I search for added service <service>
	## Then I verify the confirm <status> of funding information
	Then I verify the # of teacher and co-educator
	##And I logout of Kim crm application
	##And I close the browser successfully
Examples: 
	| service            | status  |
	| NCL SP110719_80256 | Pending |