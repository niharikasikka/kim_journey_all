﻿Feature: SmokeTest20_TravelAllowance_ApplicationFunding_NonRural_Task
	Travel Allowance submit via K1 as service provider
	Both the services should be non rural
	Run the appropriate job
	Check the travel allowance in Kim crm and complete tasks
	Review TAF-2 Travel Allowance – application for funding – non-rural service

##########################################################################################################################################
###
##Please note: Travel Allowance checkbox is checked for the service provider from Kim sharepoint aplication
###
##Also the teacher travelling between services should be added to both the service
###
##
###Both the services should be confirmed
##########################################################################################################################################

###Update the ServiceTravel depending on the service provider used in your test
###Service name should be updated based on test performed
###Name refers to teacher name so should be updated accordingly
Scenario Outline: 01 ApplicationTravelAllowance_Kim1_NonRural
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I open an existing teacher to apply for Travel Allowance with <name>
	And  I fill teachers application for travel leave
	| ServiceTravel | FirstDay_Travel | Weeks_Year1 | Weeks_Year2 | Kms | Name         | Position       |
	| NS Service    | " 22/10/2019 "  | 5           | 4           | 10  | Scod Martin | Senior Manager | 
	And I close the browser successfully
	Examples:
	| service    | name |
	| NC Service | O OO | 

###To initiate the travel allowance leave job
###The job imports the travel leave to kim crm
Scenario: 02 TravelAllowance_Job
	Given I go to manage job url
	When I run the travel leave job
	Then the service is successfully run
	And I logout of manage jobs
	And I close the browser successfully

###As RO Kim approver review and approve the TAF-2 task
###TAF-2 task will only appear if both the services the teacher is travelling to and from are standard type and non rural
###
#### below are the corresponding value for each action
	## 864710007 - Approve
	## 864710005 - More Information
	## 864710002 - Reject to Region
	## 864710001 - Request Central Office Approval

###Wait for specified miliseconds
Scenario Outline: 03 WaitScript
    Given I wait for <fewMins> mins
Examples: 
	| fewMins |
	| 100000   | 

###Verify in kim crm that correct travel allowance pre commitment is created
Scenario Outline: 04 KimCrm_TravelAllowance_RO_KimApprover_WithTask
	Given I login to Kim crm application as RO kim approver
	When As a Kim approver I search for added service <service>
	Then I approve the following precommitments and funding category with task <task> and <note>
	| Funding Category | Action    |
	| Travel Allowance | 864710007 | 	
	##And I logout of Kim crm application
	And I close the browser successfully
Examples: 
	| service    | task                                                           | note                    |
	| NC Service | Travel Allowance – application for funding – non-rural service | Approved pre commitment | 