﻿Feature: SmokeTest12_RemoveService_Rejected_CeaseDateIncorrect_Task
    Remove a funded service from kim sharepoint with past cease date
	Run the necessary jobs to get messages in crm
	Review CSK-2 (EC Service – Request to remove a service rejected – cease date incorrect) as kim region editor

	#######################################################################################################
	##
	##Pre conditions:
	##
	##A funded service with past cease date of service
	##And there are funded child records for the current year
	##
	##
	##Please note: Cease date should be the past date so that the request to cease the service is rejected
	#######################################################################################################

###Remove a non service with cease date as past date
Scenario Outline: 01 DeleteService_CeaseServiceTask
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify that the service <service> can be searched
	And I delete a <service>
	And I verify that the service <service> is deleted successfully with past date
	And I close the browser successfully
	Examples:
	| service    |
	|NS Service |  

###Manage jobs url to get the sharepoint to crm messages
Scenario: 02 RunJob_MessagesKimCrm
	Given I go to manage job url
	When I run the services batch job
	Then the service is successfully run
	And I logout of manage jobs
	And I close the browser successfully

###RO Kim editor is informed of cease service request rejected and mark the task complete
Scenario Outline: 03 KimCrm_CeaseService_RO_KimEditor_WithTask
	Given I login to Kim crm application as RO kim editor
	When As a Kim editor I search for added service <service>
	When Kim RO editor completes the task with <note>
	Then Kim editor marks the <task> complete
	##Then I logout of Kim crm application
	And I close the browser successfully
	Examples: 
	| service    | note                      | task                                                        |
	|NS Service | Cease service is rejected | Request to remove a service rejected – cease date incorrect |  

##As regional Kim approver status update to approve
Scenario Outline: 04 KimCrm_AnnualConfTasks_RO_KimApprover
	Given I login to Kim crm application as RO kim approver
	When As a Kim approver I search for added service <service>
	And Kim approver checks for data and changes the status to Approve
	##Then I logout of Kim crm application
	Then I close the browser successfully
	Examples:
	| service    |
	|NS Service |  