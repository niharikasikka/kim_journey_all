﻿Feature: SmokeTest14_ParentalLeave_YBSKTeacher
##########################################################################################################################################################################################################################################################################################################################################################################################################
## This test automation is to verify the following
## (1) Teacher can apply for Parental Leave
## (2) Parental Leave is in KIM2
## (3) Parental Leave in KIM2 can be approved
## (4) Parental Leave Application became available again once the leave has been approved
##
## THINGS TO DO/NOTE:
## Teacher who will apply the parental leave for Moonee Valley City Council which is setup in Resource.resx > ServiceProvider
## Edit atleast the Date From and Date To (which should be a past date) in the table below
## Build/Rebuild
##########################################################################################################################################################################################################################################################################################################################################################################################################
@smoke
Scenario Outline: 01 ApplicationParentalLeave
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I open an existing teacher to apply for parentalleave  with <name>
	And  I fill teachers application for parental leave
	| Date From    | Date To       | HourlyRate | Weekly Hours1 | Weekly Hours2 | Name         | Position       |
	| "22/06/2019" | " 23/06/2019" | 50         | 40            | 40            | Scott Martin | Senior Manager |
	And I close the browser successfully
	Examples:
	| service      | name                            |
	| NCL SP270601 | Teacher27060252 William27060252 |


@smoke
Scenario: 02 Run parental leave job
	Given I go to manage job url
	When I run the parental leave job
	Then the service is successfully run
	And I logout of manage jobs
	And I close the browser successfully

###Wait for specified miliseconds
Scenario Outline: 03 WaitScript
    Given I wait for <few> mins
Examples: 
	| few   |
	| 60000 |

@smoke
Scenario Outline: 04 Verify parental leave and request for approval in KIM2
	Given I login to Kim crm application as system admin 
	When I search for added service <service>
	Then I verify the confirm <status> of funding information
	And I check the Workforce Parental Leave and request for manager approval
	| name                            | Funding Type   | Action    | Funding Status |
	| Teacher27060252 William27060252 | Parental Leave | 864710001 | Pending        |
	## Actions
	## 864710000 - None
	## 864710001 - Request Manager Approval
	## 864710002 - More Information
	## 864710003 - Cancel
	And I logout of Kim crm application
	And I close the browser successfully
	Examples: 
	| service      | status    |
	| NCL SP270601 | Confirmed |


@smoke
Scenario Outline: 05 Parental leave approval in KIM2
	Given I login to Kim crm application as RO kim approver
	When As a Kim approver I search for added service <service>
	Then I verify the confirm <status> of funding information
	And I approve the following precommitments and funding category
	| Funding Category | Action    |
	| Parental Leave   | 864710007 |	
	## below are the corresponding value for each action
	## 864710007 - Approve
	## 864710005 - More Information
	## 864710002 - Reject to Region
	## 864710001 - Request Central Office Approval
	And I logout of Kim crm application
	And I close the browser successfully
Examples: 
	| service      | status    |
	| NCL SP270601 | Confirmed |

@smoke
Scenario: 06 Run KIM crm message to KIM sharepoint
	Given I go to manage job url
	When I run the messages batch job
	Then the service is successfully run
	And I logout of manage jobs
	And I close the browser successfully

###Wait for specified miliseconds
Scenario Outline: 07 WaitScript
    Given I wait for <few> mins
Examples: 
	| few    |      
	| 120000 |

@smoke
Scenario Outline: 08 Check if Parental Leave Application is available
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I open an existing teacher to check if can apply for parentalleave  with <name>
	And I close the browser successfully
	Examples:
	| service      | name                            |
	| NCL SP270601 | Teacher27060252 William27060252 |