﻿Feature: SmokeTest19_AnnualConfirmation_AfterConfirmationPeriod_Task
	Search for an existing service with completed data on KIM but annual confirmation form is not submitted
	Submit annual confirmation form after confirmation period (which is somewhere in april month)
	Once annual confirmation is submitted , enrolment confirmation job is run
	Task (ECF-22	Annual confirmation – Annual confirmation after Confirmation Period) should be created for RO Kim approve to approve
	The task is reviewed with note and marked as completed

###############################################################################################################################################
###
##2 types of service can be used to test the below scenario
###
##An existing service with tecaher, educator, programs and enrolments tab completed and annual confirmation is not submitted
##Teacher: complete;Other Educatore: Complete; Programs:Complete; Enrolments: Complete; Annual Confirmation: Not Complete
###OR
##
##An existing service with incomplete tabs where we first complete the tabs using smoke test 3 and then start executing this scenario
##This scenaio depicts the existing service with incomplete tabs
###
##
##Please note the below scenario is for sessional program 4 yo only
###Also sometimes the job in second step takes more time, in that case scenario 3 wll not pass
##
###Important: Before completing the annual confirmation please check the service has no pending pre commitmnets
##If there are pending pre commitments, please complete those before running the scenario 03 KimCrmPreCommitments_ApprovePrecommitment_WithTask
##
###############################################################################################################################################

Scenario Outline: 01 VerifyService_ExistingService_CompleteTeacher
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I complete teachers tab
	| RegistrationStatus                                             | Groups | TeacherShare | IndustrialAgreement                             | Level | hours |
	| Not registered (with temporary approval and service exemption) | 4      | No           | Early Education Employees Agreement 2016 (EEEA) | 1.1   | 5     |
	And I close the browser successfully
	Examples:
	| service      |
	| NS22 Service |  
	
Scenario Outline: 02 ExistingService_CompleteEducator
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I complete educator tab <hours>
	And I close the browser successfully
	Examples:
	| service      | hours |
	| NS22 Service | "6"   |  


Scenario Outline: 03 ExistingService_CompleteProgram_Sessional
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I complete program tab 
	And I close the browser successfully
	Examples:
	| service       |
	|NS22 Service |  

###Wait time after running the jobs varies. Sometimes the jobs takes longer to complete and hence next script may fail.
###Run this job to get program option when adding an enrolment
Scenario: 04 RunAllJobs
	Given I go to manage job url
	When I run the ybsk wrapper job to import programs, child etc to kim crm
	Then the service is successfully run
	And I logout of manage jobs
	And I close the browser successfully

Scenario Outline: 05 WaitScript
    Given I wait for <few> mins
Examples: 
	| few    |      
	| 300000 |

###The job imports the messages from crm to sharepoint	
Scenario: 06 RunJob2
	Given I go to manage job url
	When I run the messages batch job
	Then the service is successfully run
	And I logout of manage jobs
	And I close the browser successfully

Scenario Outline: 07 WaitScript
    Given I wait for <few> mins
Examples: 
	| few    |      
	| 150000 |

Scenario Outline: 08 ExistingService_AddEnrolments_YBSK
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I add enrolments for YBSK
	| Given Name      | Family Name     | Sex    | Date of Birth   | Number          | Street          | StreetType | Suburb   | State | Postcode        | DateCommenced  | language | Immunisation                                                         | GroupAttended | AdultA High           | AdultA Qualification     | AdultA Occupation | AdultB HIgh           | AdultB Qualification     | AdultB Occupation |
	| <autogenerated> | <autogenerated> | Female | <autogenerated> | <autogenerated> | <autogenerated> | STEPS      | essendon | VIC   | <autogenerated> | " 09/07/2019 " | Dutch    | The child has an up to date immunisation status certificate recorded | Orange Group  | Year 12 or equivalent | Bachelor degree or above | A                 | Year 12 or equivalent | Bachelor degree or above | A                 |
	| <autogenerated> | <autogenerated> | Female | <autogenerated> | <autogenerated> | <autogenerated> | STEPS      | essendon | VIC   | <autogenerated> | " 09/07/2019 " | Dutch    | The child has an up to date immunisation status certificate recorded | Orange Group  | Year 12 or equivalent | Bachelor degree or above | A                 | Year 12 or equivalent | Bachelor degree or above | A                 | 
	And I close the browser successfully
	Examples:
	| service       |
	| NS22 Service | 

###To initiate the enrolment confirmation process for an EC Service
Scenario: 09 RunEnrolConfirmation_Job
	Given I go to manage job url
	When I run the enrolment confirmation job
	Then the service is successfully run
	And I logout of manage jobs
	And I close the browser successfully

###Wait for specified miliseconds
Scenario Outline: 10 WaitScript
    Given I wait for <few> mins
Examples: 
	| few    |      
	| 150000 |

###The job imports the messages from crm to sharepoint	
Scenario: 11 RunCrm_Job
	Given I go to manage job url
	When I run the messages batch job
	Then the service is successfully run
	And I logout of manage jobs
	And I close the browser successfully

Scenario Outline: 12 WaitScript
    Given I wait for <few> mins
Examples: 
	| few    |      
	| 150000 |

###Run the annual confirmation feature more appropriate to your service. It depends on type of program of the service.Choose one from annual confirmation tag.
###Run the annual confirmation scenario to confirm the date and submit it to kim crm
Scenario Outline: 13 AnnualConfirm_Sessional_4yo
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify the <service> is added successfully to the organisation in Kim sharepoint
	And I check the status for Teachers, Educators, Program, and Enrolments tabs all COMPLETE
	Then I verify the status for Annual Confirmation should be COMPLETE
	And I click on annual confirmation tab
	Then I verify the available fields in annual confirmation tab
	And I verify the values in annual confirmation tab
	And I click confirm and submit the data for sessional program
	| TLDS completed | 0 TLDS completed forwarded to schools | 3 year old children enrolled | no of hours for 3 year old | Total Other Early | Masters or above | Graduate diploma | Dual bachelors | Bachelor | Pathway | Diploma | CertName | CertPosition | CertDate   |
	| 0              | 0                                     | 0                            | 0                          | 0                 | 0                | 0                | 0              | 0        | 0       | 0       | TestName | TestPosition | 01/08/2013 |
	Then I verify the status for Teachers, Educators, Program, and Enrolments tabs all Read Only
	And I close the browser successfully	
	Examples:
	| service      |
	| NS22 Service | 

### To initiate the enrolment confirmation process for an EC Service
Scenario: 14 Run_EnrolmentConfirmationJob
	Given I go to manage job url
	When I run the enrolment confirmation job
	Then the service is successfully run
	And I logout of manage jobs
	And I close the browser successfully

Scenario Outline: 15 WaitScript
    Given I wait for <few> mins
Examples: 
	| few    |      
	| 150000 |

###The funding category table should have rows depending of type of enrolments for the service
###Run the below scenario to approve the pre commitments and complete the task: Annual confirmation – Annual confirmation after Confirmation Period
###To test ECF-22 task is created and approved as RO approver
###
###Before running this script, please check if Annual confirmation – Per Capita not within Tolerance is created
##If the task exist, add a note and mark the task complete
###
##	##| ESK EXT CP               | 864710007	|
##| Kindergarten Fee Subsidy | 864710007 |
	## below are the corresponding value for each action
	## 864710007 - Approve
	## 864710005 - More Information
	## 864710002 - Reject to Region
	## 864710001 - Request Central Office Approval
	##Then I verify the confirm <status> of funding information
Scenario Outline: 16 KimCrmPreCommitments_ApprovePrecommitment_WithTask
	Given I login to Kim crm application as RO kim approver
	When As a Kim approver I search for added service <service>  
	Then I approve the following precommitments and funding category with task <task> and <note>
	| Funding Category         | Action    |
	| Per Capita               | 864710007 |	
	And I close the browser successfully
Examples: 
	| service      | task                                          | note                    |
	| NS22 Service | Annual confirmation after Confirmation Period | Approved pre commitment |  