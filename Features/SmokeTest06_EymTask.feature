﻿Feature: SmokeTest06_EymTasks
	     Edit an existing service and apply for EYM Funding
		 Verify the tasks created in Kim Crm
############################################################################################################
## use this feature to test the tasks created for EYM service
## following features are included
#
# Editing an existing service and apply for EYM Funding
# Filling in details related to EYM in Kim sharepoint
# Run the integrated jobs to send the messages in Kim Crm
# Verify the correct tasks are created in Kim Crm
# Editor edits the information and approver approves the information
#
## Notes:
## Eym start date, Eym start funding date and end funding date should be changed as per need
## Make sure service name is same for all the scenarios listed below for end to end flow testing
## After integrated jobs are run, it may be required to wait for few more minutes to status to change in KIM 1 and vice versa
###########################################################################################################

## This feature will edit a non EYM service and apply for EYM in Kim sharepoint
## Verify the Eym funding status is "Submitted"
###THIS SCRIPT DOES NOT WORK ON IE
@Eymtasks
Scenario Outline: 01 ApplyEym_ExistingService
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify that the service <service> can be searched
	And I edit a <service> for EYM funding
	And I apply for EYM organisation
	And I verify EYM funding status for <service> is submitted
	And I close the browser successfully
	Examples:
	| service    |
	| Mock2 Service |  

## To import any Service information submitted in KIM SharePoint to Kim crm
@Eymtasks
Scenario: 02 Run KIM sharepoint to crm service
	Given I go to manage job url
	When I run the services batch job
	Then the service is successfully run
	And I logout of manage jobs
	And I close the browser successfully

###Wait for specified miliseconds
Scenario Outline: 03 WaitScript
    Given I wait for <fewMins> mins
Examples: 
	| fewMins |
	| 50000   | 

## As Kim editor - central office, complete ACS-1 task
## ACS task: EC Service – application to add an additional EYM service 
## Please note should only be run with Kim editor user details
@Eymtasks
Scenario Outline: 04 KimCrm_ACS1Task_CO_KimEditor
	Given I login to Kim crm application as CO kim editor
	When As a Kim editor I search for added service <service>
	And Kim editor completes the task and change the status
	| Eym Start Date | Eym Funding Start Date | Eym Funding End Date | Notes          | Action                          |
	| 20/09/2019     | 20/09/2019             | 20/09/2020           | Please approve | Request Central Office Approval |
	Then Kim editor marks the <task> complete
	And I logout of Kim crm application
	And I close the browser successfully
	Examples:
	| service       | task                                         |
	| NS22 Service | application to add an additional EYM service | 

## As Kim approver - central office, approve ACS-2 task
## ACS task: EC Service – EYM status has changed 
@Eymtasks
Scenario Outline: 05 KimCrm_ACS2Task_CO_KimApprover
	Given I login to Kim crm application as CO kim approver
	When As a Kim approver I search for added service <service>
	And Kim approver checks for data and changes the status to Approve
	Then Kim approver marks the <task> complete
	And I logout of Kim crm application
	And I close the browser successfully
	Examples:
	| service    | task                   |
	| NS22 Service | EYM status has changed |  

## To update the CRM Status of a record in KIM SharePoint
## Run this job to import messages from crm to sharepoint
@Eymtasks
Scenario: 06 Run KIM crm message to KIM sharepoint
	Given I go to manage job url
	When I run the messages batch job
	Then the service is successfully run
	And I logout of manage jobs
	And I close the browser successfully

###Wait for specified miliseconds
Scenario Outline: 07 WaitScript
    Given I wait for <fewMins> mins
Examples: 
	| fewMins |
	| 50000   | 

## Verify the service is Approved for EYM funding in Kim sharepoint application
## The Eym funding status of the service in Kim sharepoint is "Approved"
## This is the last verification for this smoke journey
@Eymtasks
Scenario Outline: 08 VerifyEymStatus_ExistingService
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify that the service <service> can be searched
	And I verify EYM funding status for service is Approved  
	And I close the browser successfully
	Examples:
	| service    |
	| Mock2 Service |
	
################################ END OF SCRIPT #############################################