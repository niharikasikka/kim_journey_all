﻿Feature: SmokeTest15_RemoveService_Rejected_UpdateInProgress_Task
    Remove a funded service from kim sharepoint with some update in progress for the service
	Run the necessary jobs to get messages in crm
	Review CSK-1 (EC Service – Request to remove a service rejected – update in progress) as kim region editor

	#######################################################################################################
	##
	##Pre conditions:
	##
	##A funded and confirmed service with a cease date of service
	##The service should have any update in progress or pending regional approval
	##And there are funded child records for the current year
	##
	###If a workflow (pending, assigned to region, waiting region approval, waiting central office approval) is open against the service record, 
	###Task CSK-1 is automatically raised in the Dashboard to contact the service provider.
	##
	###The following scenario creates a pending update and then removes the service to make sure the correct task in kim crm is created
	###This scenario alsomake sure the pending update is approved/completed before marking the task complete
	##
	##Please note: The service's update should be in progress and request to cease the service is rejected
	#######################################################################################################

###Adding a note to kim crm and keeping an update in progress
Scenario Outline: 01 KimCrm_RO_KimEditor_AddingNote
	Given I login to Kim crm application as RO kim editor
	When As a Kim editor I search for added service <service>
	When Kim RO editor completes the task with <note>
	##Then I logout of Kim crm application
	Then I close the browser successfully
	Examples: 
	| service       | note                    |
	| Mock2 Service | Update in progress task | 

###Remove a non service with cease date as past date
Scenario Outline: 02 DeleteService_CeaseServiceTask
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify that the service <service> can be searched
	And I delete a <service>
	And I verify that the service <service> is deleted successfully
	And I close the browser successfully
	Examples:
	| service    |
	| Mock2 Service | 

###Manage jobs url to get the sharepoint to crm messages
Scenario: 03 RunJob_MessagesKimCrm
	Given I go to manage job url
	When I run the services batch job
	Then the service is successfully run
	And I logout of manage jobs
	And I close the browser successfully

###Check if task is appropriately created
Scenario Outline: 04 KimCrm_RO_KimEditor_Verify_TaskCreated
	Given I login to Kim crm application as RO kim editor
	When As a Kim editor I search for added service <service>
	Then Kim editor verifies the <task> created
	##Then I logout of Kim crm application
	And I close the browser successfully
	Examples: 
	| service    | task                                                      |
	| Mock2 Service | Request to remove a service rejected – update in progress |  

###Complete the update in order to mark the task complete as RO Kim Approver
Scenario Outline: 05 KimCrm_AnnualConfTasks_RO_KimApprover
	Given I login to Kim crm application as RO kim approver
	When As a Kim approver I search for added service <service>
	And Kim approver checks for data and changes the status to Approve
	##Then I logout of Kim crm application
	Then I close the browser successfully
	Examples:
	| service    |
	| Mock2 Service |  

###RO Kim editor is informed of cease service request rejected and mark the task complete
Scenario Outline: 06 KimCrm_CeaseService_RO_KimEditor_WithTask
	Given I login to Kim crm application as RO kim editor
	When As a Kim editor I search for added service <service>
	When Kim RO editor completes the task with <note>
	Then Kim editor marks the <task> complete
	##Then I logout of Kim crm application
	And I close the browser successfully
	Examples: 
	| service    | note                      | task                                                      |
	| Mock2 Service| Cease service is rejected | Request to remove a service rejected – update in progress |  

##As regional Kim approver status update to approve
Scenario Outline: 07 KimCrm_AnnualConfTasks_RO_KimApprover
	Given I login to Kim crm application as RO kim approver
	When As a Kim approver I search for added service <service>
	And Kim approver checks for data and changes the status to Approve
	##Then I logout of Kim crm application
	Then I close the browser successfully
	Examples:
	| service    |
	|Mock1 Test |  