﻿Feature: SmokeTest13_RemoveServiceTask_Eym
	Remove a service from kim sharepoint for an Eym funded service
	Run the necessary jobs to get messages in crm
	Review CSK-3 (EC Service – Approval of request to remove a service from K1) as kim region approver
	and
	Review CSK-4 (EC Service – A request to cease per capita funding has been approved.  Confirm if EYM funding  is to cease or continue) as CO kim editor
	Review RCS-2 EC Service – Approve Request to cease EYM Funding as CO kim approver
	###################################################################################
	##
	##Pre conditions:
	##
	##An eym funded service with appropriate cease date of service
	##And there are no funded child records for the current year
	##or
	##An eym ready for funding service
	##The service should exist in kim sharepoint
	##
	##Please note: Cease date should be current date or future date
	####################################################################################

###Remove an eym service with cease date as current date
Scenario Outline: 01 DeleteService_Eym_CeaseServiceTask
	Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify that the service <service> can be searched
	And I delete a <service>
	And I verify that the service <service> is deleted successfully
	And I close the browser successfully
	Examples:
	| service        |
	| Mock2 Service |  

###Manage jobs url to get the sharepoint to crm messages
Scenario: 02 RunJob_MessagesKimCrm
	Given I go to manage job url
	When I run the services batch job
	Then the service is successfully run
	And I logout of manage jobs
	And I close the browser successfully

###Wait for specified miliseconds
Scenario Outline: 03 WaitScript
    Given I wait for <fewMins> mins
Examples: 
	| fewMins |
	| 50000   | 

###RO Kim approver approves the service cease request and mark the task complete
Scenario Outline: 04 KimCrm_CeaseService_RO_KimApprover_WithTask
	Given I login to Kim crm application as RO kim approver
	When As a Kim approver I search for added service <service>
	And approver approves the <service> with <status>
	Then Kim approver marks the <task> complete
	And I logout of Kim crm application
	And I close the browser successfully
	Examples: 
	| service       | status  | task                                            |
	| Mock2 Service | Approve | Approval of request to remove a service from K1 | 

###As CO Kim editor cease the eym funding to the ceased service
###Change the EYM Managed Service field to 'No', enter a EYM End Date
###change the EYM Funding End Date if required, update the Action to 'Approve'
##This script updates the eym end date and eym funding end date as current date
Scenario Outline: 05 KimCrm_CSK4_CO_KimEditor
	Given I login to Kim crm application as CO kim editor
	When As a Kim editor I search for added service <service>
	And Kim editor completes the task and change the status to CO approver request
	Then Kim editor marks the <task> complete
	And I logout of Kim crm application
	And I close the browser successfully
	Examples:
	| service       | task                                                    |
	| Mock2 Service | A request to cease per capita funding has been approved |  

###As CO Kim approver, approve the request to cease eym funding
###Complete the task RCS-2 EC Service – Approve Request to cease EYM Funding
Scenario Outline: 06 KimCrm_RCS2Task_CO_KimApprover
	Given I login to Kim crm application as CO kim approver
	When As a Kim approver I search for added service <service>
	##And Kim approver checks for data and changes the status to Approve
	Then Kim approver marks the <task> complete
	And I logout of Kim crm application
	And I close the browser successfully
	Examples:
	| service       | task                                 |
	| Mock2 Service | Approve Request to cease EYM funding | 

###Run the job to get the job deleted from kim sharepoint
Scenario: 07 RunJob_CrmToSharepoint
	Given I go to manage job url
	When I run the messages batch job
	Then the service is successfully run
	And I logout of manage jobs
	And I close the browser successfully

###After the job is succesfully run verify that service is deleted successfully in kim sharepoint application
Scenario Outline: 08 Verify service does not appear in kim sharepoint application
Given I navigate to Kim sharepoint aplication homepage
	And I login using internal username and password
	When I press login button
	And I am logged in successfully
	And I click on service providers
	And I search for an organisation and click on it
	Then I verify that the service <service> can be searched
	And I verify that the service <service> is deleted from kim sharepoint
	And I close the browser successfully
	Examples:
	| service       |
	| Mock2 Service | 